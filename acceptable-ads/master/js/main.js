(function(){

  // Apply javascript specific styles
  document.documentElement.classList.remove("no-js");
  document.documentElement.classList.add("js");

  // Apply no-svg styles
  if (!document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1"))
    document.documentElement.classList.add("no-svg");

  document.addEventListener("DOMContentLoaded", function()
  {
    var sidebar = document.getElementById("sidebar");
    var sidebarOpen = document.getElementById("sidebar-open");
    var sidebarClose = document.getElementById("sidebar-close");

    sidebarOpen.addEventListener("click", function()
    {
      sidebar.classList.add("open");
    }, false);

    sidebarClose.addEventListener("click", function()
    {
      sidebar.classList.remove("open");
    }, false);
  }, false);

}());
