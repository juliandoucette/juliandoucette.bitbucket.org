# How to setup CMS on Windows 10

## Do this once

1. Download and install...
    1. The latest Python **2.7** from [https://www.python.org/](https://www.python.org/)
    2. The latest Git from [https://git-scm.com/](https://git-scm.com/)
    3. The latest Mercurial from [https://www.mercurial-scm.org/](https://www.mercurial-scm.org/)
2. Open `Git Bash` (From the start menu) (I suggest you pin this to your taskbar)
3. Run `touch .bash_profile` in Git Bash (This creates a file called .bash_profile)
4. Run `notepad .bash_profile` in Git Bash (This opens .bash_profile in notepad)
5. Paste `export PATH=$PATH:/c/Python27/:/c/Python27/scripts/;` in notepad (This enables you to run python from Git Bash)
6. Click File > Save (or CTRL+S) in notepad
7. Click File > Exit (or Alt+F4) in notepad
8. Run `exit` in Git Bash
9. Open `Git Bash` (From the start menu or pinned taskbar shortcut)
10. Run `easy_install Markdown Jinja2 Werkzeug` in Git Bash (This installs the CMS's dependencies)
11. Run `hg clone https://hg.adblockplus.org/cms/` in Git Bash (This downloads a copy of the CMS)
12. Run `hg clone https://hg.adblockplus.org/web.eyeo.com/` in Git Bash (This downloads a copy of eyeo.com)
    - NOTE: These files are being downloaded to your $HOME (user) folder (where My Documents, My Pictures, etc are)
13. Run `cms/runserver.py web.eyeo.com/` in Git Bash (This runs the CMS) 
    - NOTE: This command will appear as if it does not complete like the others. This is because it is running continuously until you stop it by pressing CTRL+C or CTRL+D.
14. Open web browser
15. Navigate to `http://localhost:5000`
    - NOTE: This local server does not work exactly the same as our production server. EG: Some URL redirections will not function, some pages that require form submission or data retrieval will not function, etc.

## Do this often

- To download another website, enter the following into Git Bash (replacing WEBSITE_URL with a website URL EG: adblockplus.org) `hg clone https://hg.adblockplus.org/web.WEBSITE_URL`
- To run another website, enter the following into Git Bash (replacing WEBSITE_PATH with a website path EG: web.adblockplus.org/) `cms/runserver.py WEBSITE_PATH`

Feel free to experiment with websites. You can always delete them and download them again. Nothing changes unless you `hg commit` and `hg push`.