(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = "<form id=\"addressbar\" class=\"addressbar\">\n  <button id=\"addressbar-favorite\" class=\"addressbar-favorite\" type=\"button\" disabled>\n    <i class=\"icon-\">favorite</i>\n  </button>\n  <input id=\"location\" class=\"location\" type=\"text\" placeholder=\"Search or enter location...\" autocomplete=\"off\" />\n  <button id=\"addressbar-cancel\" class=\"addressbar-cancel\" type=\"button\">\n    <i class=\"icon-\">cross</i>\n  </button>\n  <button id=\"addressbar-options\" class=\"addressbar-options\" type=\"button\">\n    <i class=\"icon-\">menu</i>\n  </button>\n</form>\n";

},{}],2:[function(require,module,exports){
module.exports = "<li id=\"{{id}}\" class=\"bookmark\">\n  <button class=\"bookmark-button\">\n    <span class=\"bookmark-thumb\"></span>\n    <span class=\"bookmark-title\">{{title}}</span>\n    <span class=\"bookmark-href\">{{location}}</span>\n  </button>\n</li>\n";

},{}],3:[function(require,module,exports){
module.exports = "<div id=\"bookmarks\" class=\"bookmarks screen\">\n  <div class=\"bookmarks-header\">\n    Bookmarked Pages\n  </div>\n  <div class=\"bookmarks-container\">\n    <div class=\"bookmarks-content\">\n      <div class=\"container\">\n        <ul id=\"bookmarks-list\"></ul>\n      </div>\n    </div>\n  </div>\n</div>\n";

},{}],4:[function(require,module,exports){
module.exports = "<div id=\"container\" class=\"container normalmode browserview\"></div>\n";

},{}],5:[function(require,module,exports){
module.exports = "<iframe class=\"{{id}} page\" srolling=\"true\" src=\"/html/examplepage.html?title={{title}}&showAds={{showAds}}\"></iframe>\n";

},{}],6:[function(require,module,exports){
module.exports = "<div class=\"{{id}} ghosthome page\">\n  <div class=\"container\">\n    <i class=\"icon-\">ghost</i>\n    <h1>Ghost Mode</h1>\n    <p>After closing a Ghost mode tab, all browser history, search history, web forms, cookies and temporary Internet files will no longer be stored on your device.</p>\n  </div>\n</div>\n";

},{}],7:[function(require,module,exports){
module.exports = "<div id=\"history\" class=\"history\">\n  <header>\n    <button id=\"history-toggle\" class=\"history-toggle\" type=\"button\">\n      <i class=\"icon-\">back</i>\n      <span>Recent History</span>\n    </button>\n    <button id=\"history-delete\" class=\"history-delete\" type=\"button\">\n      Delete\n    </button>\n  </header>\n  <div class=\"history-container\">\n    <div class=\"history-content\">\n      <div class=\"container\">\n        <ul id=\"history-list\" class=\"history-list\"></ul>\n      </div>\n    </div>\n  </div>\n</div>\n";

},{}],8:[function(require,module,exports){
module.exports = "<li id=\"{{id}}\" class=\"history-item\" data-location=\"{{location}}\">\n  <button class=\"history-button\">\n    <div class=\"history-thumb\"></div>\n    <span class=\"history-title\">{{title}}</span>\n    <span class=\"history-url\">{{location}}</span>\n    <a class=\"history-bookmark\">\n      <i class=\"icon-\">favorite_filled</i>\n    </a>\n  </button>\n</li>\n";

},{}],9:[function(require,module,exports){
module.exports = "<div class=\"{{id}} dashboard page\">\n  <a href=\"page1.com\">\n    <div class=\"thumb thumb-1\"></div>\n    <span>Page 1</span>\n  </a>\n  <a href=\"page2.com\">\n    <div class=\"thumb thumb-2\"></div>\n    <span>Page 2</span>\n  </a>\n</div>\n";

},{}],10:[function(require,module,exports){
module.exports = "<div id=\"options\" class=\"options\">\n  <div class=\"arrow-up\"></div>\n  <ul>\n    <li>\n      <button id=\"options-refresh\" class=\"options-refresh\" type=\"button\" disabled>\n        <i class=\"icon-\">refresh</i>\n        <span>Refresh page</span>\n      </button>\n    </li>\n    <li>\n      <button id=\"options-adblock\" class=\"options-adblock active\" type=\"button\" disabled>\n        <span class=\"icon-stack\">\n          <i class=\"icon-\">checkbox</i>\n          <i class=\"icon- check\">check</i>\n        </span>\n        <span>Ad blocking on this site</span>\n      </button>\n    </li>\n    <li>\n      <button id=\"options-bookmark\" class=\"options-bookmark\" type=\"button\" disabled>\n        <i class=\"bookmark icon-\">favorite</i>\n        <span>Bookmark page</span>\n      </button>\n    </li>\n    <li>\n      <button id=\"options-share\" class=\"options-share\" type=\"button\" disabled>\n        <i class=\"icon-\">share</i>\n        <span>Share page</span>\n      </button>\n    </li>\n    <li>\n      <button id=\"options-history\" class=\"options-history\" type=\"button\">\n        <i class=\"icon-\">history</i>\n        <span>View recent history</span>\n      </button>\n    </li>\n    <li>\n      <button id=\"options-settings\" class=\"options-settings\" type=\"button\">\n        <i class=\"icon-\">settings</i>\n        <span>Settings</span>\n      </button>\n    </li>\n  </ul>\n</div>\n";

},{}],11:[function(require,module,exports){
module.exports = "<div id=\"pages\" class=\"pages\">\n  <div class=\"normal-1 dashboard page active\">\n    <a href=\"page1.com\">\n      <div class=\"thumb thumb-1\"></div>\n      <span>Page 1</span>\n    </a>\n    <a href=\"page2.com\">\n      <div class=\"thumb thumb-2\"></div>\n      <span>Page 2</span>\n    </a>\n  </div>\n  <div class=\"ghost-1 ghosthome page\">\n    <div class=\"container\">\n      <i class=\"icon-\">ghost</i>\n      <h1>Ghost Mode</h1>\n      <p>After closing a Ghost mode tab, all browser history, search history, web forms, cookies and temporary Internet files will no longer be stored on your device.</p>\n    </div>\n  </div>\n</div>\n";

},{}],12:[function(require,module,exports){
module.exports = "<li id=\"{{id}}\", class=\"tab\">\n  <button class=\"tab-button\" type=\"button\">\n    <div class=\"tab-thumb\"></div>\n    <div class=\"tab-title\">{{title}}</div>\n    <div class=\"tab-url\">{{location}}</div>\n  </button>\n</li>\n";

},{}],13:[function(require,module,exports){
module.exports = "<div id=\"{{mode}}-tabs\" class=\"{{mode}}-tabs tabs\">\n  <div class=\"tabs-container\">\n    <div class=\"tabs-content\">\n      <ul>\n        <li>\n          <button class=\"tabs-create\" type=\"button\">\n            <div class=\"tab-thumb\"></div>\n            <div class=\"tab-title\">Add New Tab</div>\n            <div class=\"tab-url\"></div>\n          </button>\n        </li>\n      </ul>\n      <ul class=\"tabs-list\">\n        <li id=\"{{mode}}-1\", class=\"tab\">\n          <button class=\"tab-button\" type=\"button\">\n            <div class=\"tab-thumb\"></div>\n            <div class=\"tab-title\">New Tab</div>\n            <div class=\"tab-url\"></div>\n          </button>\n        </li>\n      </ul>\n    </div>\n  </div>\n</div>\n";

},{}],14:[function(require,module,exports){
module.exports = "<div id=\"tapbar\" class=\"tapbar\">\n  <ul>\n    <li>\n      <button id=\"tapbar-back\" type=\"button\" class=\"tapbar-back\" disabled>\n        <i class=\"icon-\">back</i>\n      </button>\n    </li>\n    <li>\n      <button id=\"tapbar-forward\" class=\"tapbar-forward\" type=\"button\"  disabled>\n        <i class=\"icon-\">forward</i>\n      </button>\n    </li>\n    <li>\n      <button id=\"tapbar-normal-tabs\" class=\"tapbar-normal-tabs\" type=\"button\">\n        <i class=\"icon-\">tabs_filled</i>\n        <span>1</span>\n      </button>\n    </li>\n    <li>\n      <button id=\"tapbar-ghost-tabs\" class=\"tapbar-ghost-tabs\" type=\"button\">\n        <i class=\"icon-\">ghost</i>\n        <span>1</span>\n      </button>\n    </li>\n    <li>\n      <button id=\"tapbar-bookmarks\" class=\"tapbar-bookmarks\" type=\"button\">\n        <i class=\"bookmark icon-\">bookmark</i>\n        <i class=\"star icon-\">favorite_filled</i>\n      </button>\n    </li>\n  </ul>\n</div>\n";

},{}],15:[function(require,module,exports){
/*!
 * @source /js/controllers/addressbar.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _state = require('../models/state');
var _pages = require('../models/pages');
var _bookmarks = require('../models/bookmarks');
var _tabs = require('../models/tabs');

var _view = require('../views/addressbar');

var _getActivePage = require('../helpers/getActivePage');
var _toggleBookmark = require('../helpers/toggleBookmark');
var _gotoLocation = require('../helpers/gotoLocation');
var _onNavigate = require('../helpers/onNavigate');

var controller = {};

//= HANDLE VIEW ACTIONS ======================================================//

controller.onToggleFavorite = _toggleBookmark;

_view.onToggleFavorite.connect(controller.onToggleFavorite);

controller.onSubmit = function(location)
{
  _gotoLocation(location);
  _view.blur();
};

_view.onSubmit.connect(controller.onSubmit);

controller.onCancel = function()
{
  _view.setLocation(_getActivePage().location);
  _view.deselectLocation();
};

_view.onBlur.connect(controller.onCancel);
_view.onCancel.connect(controller.onCancel);

controller.onFocus = function()
{
  _view.selectLocation();
};

_view.onFocus.connect(controller.onFocus);


//= HANDLE MODEL ACTIONS =====================================================//

controller.onSetView = function(view)
{
  if (view === _state.VIEW.NORMAL_TABS || view === _state.VIEW.GHOST_TABS)
  {
    _view.disable();
  }
  else
  {
    _view.enable(_getActivePage().internal);
  }
};

_state.setView.connect(controller.onSetView);

controller.onSetPage = function(tab)
{
  var page = _pages.get(tab.page);
  var bookmarked = _bookmarks.get(page);

  _view.setLocation(page.location);

  if (page.internal)
  {
    _view.disableFavorite();
  }
  else
  {
    _view.enableFavorite();
  }

  if (bookmarked)
  {
    _view.favorite();
  }
  else
  {
    _view.unfavorite();
  }
};

_onNavigate.connect(controller.onSetPage);
_tabs.switch.connect(controller.onSetPage);

controller.onCreateBookmark = function(page)
{
  if (page.id === _getActivePage().id)
  {
    _view.favorite();
  }
};

_bookmarks.create.connect(controller.onCreateBookmark);

controller.onDestroyBookmark = function(page)
{
  if (page.id === _getActivePage().id)
  {
    _view.unfavorite();
  }
};

_bookmarks.destroy.connect(controller.onDestroyBookmark);

return controller;

/*! @licend */

},{"../helpers/getActivePage":24,"../helpers/gotoLocation":26,"../helpers/onNavigate":30,"../helpers/toggleBookmark":31,"../models/bookmarks":33,"../models/pages":35,"../models/state":36,"../models/tabs":37,"../views/addressbar":44}],16:[function(require,module,exports){
/*!
 * @source /js/controllers/bookmarks.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _state = require('../models/state');
var _pages = require('../models/pages');
var _bookmarks = require('../models/bookmarks');

var _gotoLocation = require('../helpers/gotoLocation');

var _view = require('../views/bookmarks');

var controller = {};

//= HANDLE VIEW ACTIONS ======================================================//

controller.onSelect = function(id)
{
  var page = _pages.get(id);
  _gotoLocation(page.location);
  _state.setView(_state.VIEW.BROWSER);
};

_view.onSelect.connect(controller.onSelect);

controller.onDestroy = function(id)
{
  _bookmarks.destroy(_pages.get(id));
};

_view.onDestroy.connect(controller.onDestroy);

//= HANDLE MODEL ACTIONS =====================================================//

controller.onSetView = function(view)
{
  if (view === _state.VIEW.BOOKMARKS)
  {
    _view.open();
  }
  else if (_state.getLastView() === _state.VIEW.BOOKMARKS)
  {
    _view.close();
  }
};

_state.setView.connect(controller.onSetView);

controller.onCreate = function(page)
{
  _view.create(page.id, page.title, page.location);
};

_bookmarks.create.connect(controller.onCreate);

controller.onDestroy = function(page)
{
  _view.destroy(page.id);
};

_bookmarks.destroy.connect(controller.onDestroy);

module.exports = controller;

/*! @licend */

},{"../helpers/gotoLocation":26,"../models/bookmarks":33,"../models/pages":35,"../models/state":36,"../views/bookmarks":45}],17:[function(require,module,exports){
/*!
 * @source /js/controllers/splashscreen.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _state = require('../models/state');
var _tabs = require('../models/tabs');
var _pages = require('../models/pages');
var _whitelist = require('../models/whitelist');
var _bookmarks = require('../models/bookmarks');

var _getActivePage = require('../helpers/getActivePage');
var _onNavigate = require('../helpers/onNavigate');

var _container = require('../views/container');

var controller = {};

//= HANDLE VIEW ACTIONS ======================================================//

controller.construct = function()
{
  var addressbar = require('../views/addressbar');
  var options = require('../views/options');
  var pages = require('../views/pages');
  var tabs = require('../views/tabs');
  var bookmarks = require('../views/bookmarks');
  var history = require('../views/history');
  var tapbar = require('../views/tapbar');

  _container.el.appendChild(addressbar.el);
  _container.el.appendChild(options.el);
  _container.el.appendChild(pages.el);
  _container.el.appendChild(tabs.normal);
  _container.el.appendChild(tabs.ghost);
  _container.el.appendChild(bookmarks.el);
  _container.el.appendChild(history.el);
  _container.el.appendChild(tapbar.el);
  document.body.appendChild(_container.el);
};

if (document.readyState === 'complete')
{
  controller.construct();
}
else
{
  document.addEventListener('DOMContentLoaded', controller.construct);
}

//= HANDLE MODEL ACTIONS =====================================================//

controller.onSetMode = function(mode)
{
  _container.setMode(mode, _state.getLastMode());
};

_state.setMode.connect(controller.onSetMode);

controller.onSetView = function(view)
{
  _container.setView(view, _state.getLastView());
};

_state.setView.connect(controller.onSetView);

controller.onToggleAdblock = function(page)
{
  if (_getActivePage().id === page.id)
  {
    _container.setWhitelisted(_whitelist.get(page));
  }
};

_whitelist.create.connect(controller.onToggleAdblock);
_whitelist.destroy.connect(controller.onToggleAdblock);

controller.onToggleBookmark = function(page)
{
  if (_getActivePage().id === page.id)
  {
    _container.setBookmarked(_bookmarks.get(page));
  }
};

_bookmarks.create.connect(controller.onToggleBookmark);
_bookmarks.destroy.connect(controller.onToggleBookmark);

controller.onChangeTab = function(tab)
{
  var page = _pages.get(tab.page);
  var whitelisted = _whitelist.get(page);
  var bookmarked = _bookmarks.get(page);
  _container.setBookmarked(bookmarked);
  _container.setWhitelisted(whitelisted);
};

_tabs.switch.connect(controller.onChangeTab);
_onNavigate.connect(controller.onChangeTab);

/*! @licend */

},{"../helpers/getActivePage":24,"../helpers/onNavigate":30,"../models/bookmarks":33,"../models/pages":35,"../models/state":36,"../models/tabs":37,"../models/whitelist":38,"../views/addressbar":44,"../views/bookmarks":45,"../views/container":46,"../views/history":47,"../views/options":48,"../views/pages":49,"../views/tabs":51,"../views/tapbar":52}],18:[function(require,module,exports){
/*!
 * @source /js/controllers/history.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _state = require('../models/state');
var _history = require('../models/history');
var _tabs = require('../models/tabs');
var _pages = require('../models/pages');
var _bookmarks = require('../models/bookmarks');

var _gotoLocation = require('../helpers/gotoLocation');

var _view = require('../views/history');
var _options = require('../views/options');

var controller = {};

//= STUPID ACTIONS ===========================================================//

controller.onCreateBookmark = function(page)
{
  _view.setBookmark(page.location, true);
};

_bookmarks.create.connect(controller.onCreateBookmark);

controller.onDestroyBookmark = function(page)
{
  _view.setBookmark(page.location, false);
};

_bookmarks.destroy.connect(controller.onDestroyBookmark);

//= HANDLE VIEW ACTIONS ======================================================//

controller.onToggleHistory = function()
{
  _state.setView(_state.VIEW.HISTORY);
};

_options.onToggleHistory.connect(controller.onToggleHistory);

controller.onBack = function()
{
  _state.setView(_state.getLastView());
};

_view.onBack.connect(controller.onBack);

controller.onDelete = function()
{
  _history.delete();
};

_view.onDelete.connect(controller.onDelete);

controller.onSelect = function(id)
{
  var item = _history.get(id);

  if (item)
  {
    _gotoLocation(item.location);
    _state.setView(_state.VIEW.BROWSER);
  }
};

_view.onSelect.connect(controller.onSelect);

controller.onDestroy = function(id)
{
  var item = _history.get(id);

  if (item)
  {
    _history.destroy(item);
  }
};

_view.onDestroy.connect(controller.onDestroy);

//= HANDLE MODEL ACTIONS =====================================================//

controller.onSetPage = function(tab)
{
  var page = _pages.get(tab.page);

  if (!page.internal && _state.getMode() !== _state.MODE.GHOST)
  {
    _history.create(page);
  }
};

_tabs.setPage.connect(controller.onSetPage);

controller.onSetView = function(view)
{
  if (view === _state.VIEW.HISTORY)
  {
    _view.open();
  }
  else if (_state.getLastView() === _state.VIEW.HISTORY)
  {
    _view.close();
  }
};

_state.setView.connect(controller.onSetView);

controller.onCreate = function(item)
{
  _view.create(item.id, item.title, item.location);
};

_history.create.connect(controller.onCreate);

controller.onDestroy = function(item)
{
  _view.destroy(item.id);
};

_history.destroy.connect(controller.onDestroy);

module.exports = controller;

/*! @licend */

},{"../helpers/gotoLocation":26,"../models/bookmarks":33,"../models/history":34,"../models/pages":35,"../models/state":36,"../models/tabs":37,"../views/history":47,"../views/options":48}],19:[function(require,module,exports){
/*!
 * @source /js/controllers/options.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _whitelist = require('../models/whitelist');
var _bookmarks = require('../models/bookmarks');
var _pages = require('../models/pages');
var _tabs = require('../models/tabs');

var _options = require('../views/options');
var _addressbar = require('../views/addressbar');

var _getActiveTab = require('../helpers/getActiveTab');
var _getActivePage = require('../helpers/getActivePage');
var _isBookmarked = require('../helpers/isBookmarked');
var _isWhitelisted = require('../helpers/isWhitelisted');
var _onNavigate = require('../helpers/onNavigate');

var _isOpen = false;

var controller = {};

controller.onToggleOptions = function(event)
{
  event.stopPropagation();
  if (_isOpen)
  {
    _options.close();
    _isOpen = false;
  }
  else
  {
    _options.open();
    _isOpen = true;
  }
};

_addressbar.onToggleOptions.connect(controller.onToggleOptions);

controller.onToggleAdblock = function()
{
  var page = _getActivePage();
  if (_isWhitelisted())
  {
    _whitelist.destroy(page);
  }
  else
  {
    _whitelist.create(page);
  }
};

_options.onToggleAdblock.connect(controller.onToggleAdblock);

controller.onToggleFavorite = function()
{
  var page = _getActivePage();
  if (_isBookmarked())
  {
    _bookmarks.destroy(page);
  }
  else
  {
    _bookmarks.create(page);
  }
};

_options.onToggleFavorite.connect(controller.onToggleFavorite);

controller.onCreateBookmark = function(page)
{
  var activePage = _getActivePage();

  if (activePage.id === page.id)
  {
    _options.favorite();
  }
};

_bookmarks.create.connect(controller.onCreateBookmark);

controller.onDestroyBookmark = function(page)
{
  var activePage = _getActivePage();

  if (activePage.id === page.id)
  {
    _options.unfavorite();
  }
};

_bookmarks.destroy.connect(controller.onDestroyBookmark);

controller.onTabChange = function(tab)
{
  var activeTab = _getActiveTab();

  if (activeTab && activeTab.id === tab.id)
  {
    _options.update(_pages.get(tab.page));
    if (_isBookmarked())
    {
      _options.favorite();
    }
    else
    {
      _options.unfavorite();
    }
  }
};

_onNavigate.connect(controller.onTabChange);
_tabs.switch.connect(controller.onTabChange);

controller.onBlur = function()
{
  if (_isOpen)
  {
    _options.close();
    _isOpen = false;
  }
};

_options.onBlur.connect(controller.onBlur);

module.exports = controller;

/* @licend */

},{"../helpers/getActivePage":24,"../helpers/getActiveTab":25,"../helpers/isBookmarked":27,"../helpers/isWhitelisted":29,"../helpers/onNavigate":30,"../models/bookmarks":33,"../models/pages":35,"../models/tabs":37,"../models/whitelist":38,"../views/addressbar":44,"../views/options":48}],20:[function(require,module,exports){
/*!
 * @source /js/controllers/pages.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _tabs = require('../models/tabs');
var _pages = require('../models/pages');
var _whitelist = require('../models/whitelist');

var _gotoLocation = require('../helpers/gotoLocation');
var _getActiveTab = require('../helpers/getActiveTab');
var _onNavigate = require('../helpers/onNavigate');

var _view = require('../views/pages');

var controller = {};

//= HANDLE VIEW ACTIONS ======================================================//

controller.onNavigate = function(location)
{
  _gotoLocation(location);
};

_view.onNavigate.connect(controller.onNavigate);

//= HANDLE MODEL ACTIONS =====================================================//

controller.onToggleAdblock = function(page)
{
  var tab = _getActiveTab();
  _view.change(tab.id, page.title, page.template, _whitelist.get(page));
};

_whitelist.create.connect(controller.onToggleAdblock);
_whitelist.destroy.connect(controller.onToggleAdblock);

controller.onCreate = function(tab)
{
  var page = _pages.get(tab.page);
  _view.create(tab.id, page.title, page.template, _whitelist.get(page));
};

_tabs.create.connect(controller.onCreate);

controller.onDestroy = function(tab)
{
  _view.destroy(tab.id);
};

_tabs.destroy.connect(controller.onDestroy);

controller.onSwitch = function(tab)
{
  _view.switch(tab.id);
};

_tabs.switch.connect(controller.onSwitch);

controller.onSetPage = function(tab)
{
  var page = _pages.get(tab.page);
  _view.change(tab.id, page.title, page.template, _whitelist.get(page));
};

_onNavigate.connect(controller.onSetPage);

//= INITIALIZATION ===========================================================//



module.exports = controller;

/*! @licend */

},{"../helpers/getActiveTab":25,"../helpers/gotoLocation":26,"../helpers/onNavigate":30,"../models/pages":35,"../models/tabs":37,"../models/whitelist":38,"../views/pages":49}],21:[function(require,module,exports){
/*!
 * @source /js/controllers/splashscreen.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _splashscreen = require('../views/splashscreen');

var controller = {

  onDelayComplete: function()
  {
    _splashscreen.destroy();
  }

};

setTimeout(controller.onDelayComplete, 1500);

/*! @licend */

},{"../views/splashscreen":50}],22:[function(require,module,exports){
/*!
 * @source /js/controllers/tabs.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _state = require('../models/state');
var _tabs = require('../models/tabs');
var _pages = require('../models/pages');

var _view = require('../views/tabs');

var _onNavigate = require('../helpers/onNavigate');

var controller = {};

//= HANDLE VIEW ACTIONS ======================================================//

controller.onCreate = function()
{
  _tabs.switch(_tabs.create());
  _state.setView(_state.VIEW.BROWSER);
};

_view.onCreate.connect(controller.onCreate);

controller.onDestroy = function(id)
{
  _tabs.destroy(_tabs.get(id));
};

_view.onDestroy.connect(controller.onDestroy);

controller.onSelect = function(id)
{
  _tabs.switch(_tabs.get(id));
  _state.setView(_state.VIEW.BROWSER);
};

_view.onSelect.connect(controller.onSelect);

//= HANDLE MODEL ACTIONS =====================================================//

controller.onSetMode = function(mode)
{
  if (_state.getLastMode() !== mode)
  {
    _tabs.switch(_tabs.getMostRecent(mode === _state.MODE.GHOST));
  }
};

_state.setMode.connect(controller.onSetMode);

controller.onSetView = function(view)
{
  if (_state.getLastView() === _state.VIEW.GHOST_TABS)
  {
    _view.close(true);
  }
  else if (_state.getLastView() === _state.VIEW.NORMAL_TABS)
  {
    _view.close(false);
  }

  if (view === _state.VIEW.NORMAL_TABS)
  {
    _view.open(false);
  }
  else if (view === _state.VIEW.GHOST_TABS)
  {
    _view.open(true);
  }
};

_state.setView.connect(controller.onSetView);

controller.onCreateTab = function(tab)
{
  _view.createTab(tab, _pages.get(tab.page));
};

_tabs.create.connect(controller.onCreateTab);

controller.forceActiveTab = function()
{
  var activeTab, recentTab;

  activeTab = _tabs.getActive();

  if (!activeTab)
  {
    recentTab = _tabs.getMostRecent();

    if (!recentTab)
    {
      _tabs.switch(_tabs.create());
    }
    else
    {
      _tabs.switch(recentTab);
    }
  }
};

controller.onDestroyTab = function(tab)
{
  _view.destroyTab(tab);
  controller.forceActiveTab();
};

_tabs.destroy.connect(controller.onDestroyTab);

controller.onChangeTab = function(tab)
{
  var page = _pages.get(tab.page);
  _view.updateTab(tab, page);
};

_onNavigate.connect(controller.onChangeTab);

module.exports = controller;

/*! @licend */

},{"../helpers/onNavigate":30,"../models/pages":35,"../models/state":36,"../models/tabs":37,"../views/tabs":51}],23:[function(require,module,exports){
/*!
 * @source /js/controllers/addressbar.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _state = require('../models/state');
var _tabs = require('../models/tabs');

var _getActiveTab = require('../helpers/getActiveTab');
var _onNavigate = require('../helpers/onNavigate');

var _tapbar = require('../views/tapbar');

var controller = {};

// HANDLE VIEW ACTIONS =======================================================//

controller.onBack = function()
{
  var tab = _getActiveTab();
  _tabs.goBack(tab);
};

_tapbar.onBack.connect(controller.onBack);

controller.onForward = function()
{
  var tab = _getActiveTab();
  _tabs.goForward(tab);
};

_tapbar.onForward.connect(controller.onForward);

controller.onToggleNormalTabs = function()
{
  var fromMode = _state.getMode();

  if (fromMode !== _state.MODE.NORMAL)
  {
    _state.setMode(_state.MODE.NORMAL);
  }

  var fromView = _state.getView();

  if (
    fromMode !== _state.MODE.NORMAL ||
    fromView === _state.VIEW.NORMAL_TABS ||
    fromView === _state.VIEW.BOOKMARKS
  ) {
    _state.setView(_state.VIEW.BROWSER);
  }
  else
  {
    _state.setView(_state.VIEW.NORMAL_TABS);
  }
};

_tapbar.onToggleNormalTabs.connect(controller.onToggleNormalTabs);

controller.onToggleGhostTabs = function()
{
  var fromMode = _state.getMode();

  if (fromMode !== _state.MODE.GHOST)
  {
    _state.setMode(_state.MODE.GHOST);
  }

  var fromView = _state.getView();

  if (
    fromMode !== _state.MODE.GHOST ||
    fromView === _state.VIEW.GHOST_TABS ||
    fromView === _state.VIEW.BOOKMARKS
  ) {
    _state.setView(_state.VIEW.BROWSER);
  }
  else
  {
    _state.setView(_state.VIEW.GHOST_TABS);
  }
};

_tapbar.onToggleGhostTabs.connect(controller.onToggleGhostTabs);

controller.onToggleBookmarks = function()
{
  if (_state.getView() === _state.VIEW.BOOKMARKS)
  {
    _state.setView(_state.VIEW.BROWSER);
  }
  else
  {
    _state.setView(_state.VIEW.BOOKMARKS);
  }
};

_tapbar.onToggleBookmarks.connect(controller.onToggleBookmarks);

//= HANDLE MODEL ACTIONS =====================================================//

controller.onNavigate = function(tab)
{
  if (tab.history.length > 1 && tab.historyIndex > 0)
  {
    _tapbar.enableBack();
  }
  else
  {
    _tapbar.disableBack();
  }

  if (tab.historyIndex !== tab.history.length - 1)
  {
    _tapbar.enableForward();
  }
  else
  {
    _tapbar.disableForward();
  }
}

_onNavigate.connect(controller.onNavigate);

controller.onChangeTabsLength = function(tab)
{
  if (tab.ghost)
  {
    _tapbar.setGhostTabsLength(_tabs.getLength(tab.ghost));
  }
  else
  {
    _tapbar.setNormalTabsLength(_tabs.getLength(tab.ghost));
  }
};

_tabs.create.connect(controller.onChangeTabsLength);
_tabs.destroy.connect(controller.onChangeTabsLength);

controller.onChangeView = function(view)
{
  if (view === _state.VIEW.BOOKMARKS)
  {
    _tapbar.selectBookmarks();
  }
  else if (_state.getMode() === _state.MODE.GHOST)
  {
    _tapbar.selectGhostTabs();
  }
  else
  {
    _tapbar.selectNormalTabs();
  }
};

_state.setView.connect(controller.onChangeView);

module.exports = controller;

/* @licend */

},{"../helpers/getActiveTab":25,"../helpers/onNavigate":30,"../models/state":36,"../models/tabs":37,"../views/tapbar":52}],24:[function(require,module,exports){
/*!
 * @source /js/helpers/getActiveTab.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _pages = require('../models/pages');
var _getActiveTab = require('./getActiveTab');

module.exports = function getActivePage()
{
  return _pages.get(_getActiveTab().page);
};

/*! @licend */

},{"../models/pages":35,"./getActiveTab":25}],25:[function(require,module,exports){
/*!
 * @source /js/helpers/getActiveTab.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _state = require('../models/state');
var _tabs = require('../models/tabs');

module.exports = function getActiveTab()
{
  return _tabs.getActive(_state.getMode() === _state.MODE.GHOST);
};

/*! @licend */

},{"../models/state":36,"../models/tabs":37}],26:[function(require,module,exports){
/*!
 * @source /js/helpers/gotoLocation.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _getActiveTab = require('./getActiveTab');
var _tabs = require('../models/tabs');
var _pages = require('../models/pages');

module.exports = function gotoLocation(location)
{
  var page = _pages.get(location) || _pages.create();
  _tabs.setPage(_getActiveTab(), page);
};

/*! @licend */

},{"../models/pages":35,"../models/tabs":37,"./getActiveTab":25}],27:[function(require,module,exports){
/*!
 * @source /js/helpers/isBookmarked.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _bookmarks = require('../models/bookmarks');
var _getActivePage = require('./getActivePage');

module.exports = function isBookmarked()
{
  return _bookmarks.get(_getActivePage()) ? true : false;
};

/*! @licend */

},{"../models/bookmarks":33,"./getActivePage":24}],28:[function(require,module,exports){

var _state = require('../models/state');

module.exports = function isGhostMode()
{
  return _state.getMode() === _state.MODE.GHOST;
};

},{"../models/state":36}],29:[function(require,module,exports){
/*!
 * @source /js/helpers/isWhitelisted.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _whitelist = require('../models/whitelist');
var _getActivePage = require('./getActivePage');

module.exports = function isWhitelisted()
{
  return _whitelist.get(_getActivePage()) ? true : false;
};

/*! @licend */

},{"../models/whitelist":38,"./getActivePage":24}],30:[function(require,module,exports){

var _connectable = require('../utilities/connectable');

var _tabs = require('../models/tabs');

var onNavigate = _connectable();

_tabs.setPage.connect(onNavigate);
_tabs.goBack.connect(onNavigate);
_tabs.goForward.connect(onNavigate);

module.exports = onNavigate;

},{"../models/tabs":37,"../utilities/connectable":41}],31:[function(require,module,exports){
/*!
 * @source /js/helpers/toggleBookmark.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _bookmarks = require('../models/bookmarks');
var _getActivePage = require('./getActivePage');
var _isBookmarked = require('./isBookmarked');

module.exports = function toggleBookmark()
{
  if (_isBookmarked())
  {
    _bookmarks.destroy(_getActivePage());
  }
  else
  {
    _bookmarks.create(_getActivePage());
  }
};

/*! @licend */

},{"../models/bookmarks":33,"./getActivePage":24,"./isBookmarked":27}],32:[function(require,module,exports){
/*!
 * @source /js/main.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

window.abb = {
  controllers: {
    addressbar: require('./controllers/addressbar'),
    bookmarks: require('./controllers/bookmarks'),
    container: require('./controllers/container'),
    history: require('./controllers/history'),
    options: require('./controllers/options'),
    pages: require('./controllers/pages'),
    splashscreen: require('./controllers/splashscreen'),
    tabs: require('./controllers/tabs'),
    tapbar: require('./controllers/tapbar')
  },
  models: {
    bookmarks: require('./models/bookmarks'),
    history: require('./models/history'),
    pages: require('./models/pages'),
    state: require('./models/state'),
    tabs: require('./models/tabs'),
    whitelist: require('./models/whitelist')
  },
  views: {
    addressbar: require('./views/addressbar'),
    bookmarks: require('./views/bookmarks'),
    container: require('./views/container'),
    history: require('./views/history'),
    options: require('./views/options'),
    pages: require('./views/pages'),
    splashscreen: require('./views/splashscreen'),
    tabs: require('./views/tabs'),
    tapbar: require('./views/tapbar')
  }
};

var _bookmarks = require('./models/bookmarks');
var _history = require('./models/history');
var _pages = require('./models/pages');

_bookmarks.create(_pages.get('page1.com'));
_bookmarks.create(_pages.get('page2.com'));
_history.create(_pages.get('page1.com'));
_history.create(_pages.get('page2.com'));

/*! @licend */

},{"./controllers/addressbar":15,"./controllers/bookmarks":16,"./controllers/container":17,"./controllers/history":18,"./controllers/options":19,"./controllers/pages":20,"./controllers/splashscreen":21,"./controllers/tabs":22,"./controllers/tapbar":23,"./models/bookmarks":33,"./models/history":34,"./models/pages":35,"./models/state":36,"./models/tabs":37,"./models/whitelist":38,"./views/addressbar":44,"./views/bookmarks":45,"./views/container":46,"./views/history":47,"./views/options":48,"./views/pages":49,"./views/splashscreen":50,"./views/tabs":51,"./views/tapbar":52}],33:[function(require,module,exports){
/*!
 * @source /js/models/bookmarks.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _connectable = require('../utilities/connectable');

var _store = {};

module.exports = {

  create: _connectable(function(page)
  {
    return _store[page.id] = page;
  }),

  destroy: _connectable(function(page)
  {
    delete _store[page.id];
    return page;
  }),

  get: function(page)
  {
    return _store[page.id];
  }

};

/*! @licend */

},{"../utilities/connectable":41}],34:[function(require,module,exports){
/*!
 * @source /js/models/history.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _connectable = require('../utilities/connectable');

var _store = {};
var _count = 0;

module.exports = {

  create: _connectable(function(page)
  {
    var id = 'HISTORY_' + _count++;
    return _store[id] = {
      id: id,
      title: page.title,
      location: page.location
    };
  }),

  destroy: _connectable(function(item)
  {
    delete _store[item.id];
    return item;
  }),

  delete: _connectable(function()
  {
    for (var item in _store)
    {
      this.destroy(_store[item]);
    }
  }),

  get: function(id)
  {
    return _store[id];
  }

};

/*! @licend */

},{"../utilities/connectable":41}],35:[function(require,module,exports){
/*!
 * @source /js/models/pages.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _connectable = require('../utilities/connectable');

var _defaults = {
  id: 'page0.com',
  title: 'Page 0',
  location: 'page0.com',
  internal: false,
  template: 'examplepage'
};

var _store = {
  'normalhome': {
    id: 'normalhome',
    title: 'New Tab',
    location: '',
    internal: true,
    template: 'normalhome'
  },
  'ghosthome': {
    id: 'ghosthome',
    title: 'New Tab',
    location: '',
    internal: true,
    template: 'ghosthome'
  },
  'page1.com': {
    id: 'page1.com',
    title: 'Page 1',
    location: 'page1.com',
    internal: false,
    template: 'examplepage'
  },
  'page2.com': {
    id: 'page2.com',
    title: 'Page 2',
    location: 'page2.com',
    internal: false,
    template: 'examplepage'
  }
};

var _createPage = function()
{
  count++;

  var page = {};

  for (var value in _defaults)
  {
    if (typeof _defaults[value] === 'string')
    {
      page[value] = _defaults[value].replace('0', count);
    }
    else
    {
      page[value] = _defaults[value];
    }
  }

  return page;
};

var count = 2;

module.exports = {

  create: _connectable(function()
  {
    var page = _createPage();
    return _store[page.id] = page;
  }),

  get: function(location)
  {
    return _store[location];
  }

};

/*! @licend */

},{"../utilities/connectable":41}],36:[function(require,module,exports){
/*!
 * @source /js/models/state.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _connectable = require('../utilities/connectable');

var _store = {
  mode: 'normalmode',
  lastMode: '',
  view: 'browserview',
  lastView: '',
  isOptionsOpen: false
};

module.exports = {

  MODE: {
    NORMAL: 'normalmode',
    GHOST: 'ghostmode'
  },

  VIEW: {
    NORMAL_TABS: 'normaltabsview',
    GHOST_TABS: 'ghosttabsview',
    BOOKMARKS: 'bookmarksview',
    HISTORY: 'historyview',
    BROWSER: 'browserview'
  },

  setMode: _connectable(function(mode)
  {
    _store.lastMode = _store.mode;
    return _store.mode = mode;
  }),

  setView: _connectable(function(view)
  {
    _store.lastView = _store.view;
    return _store.view = view;
  }),

  getMode: function()
  {
    return _store.mode;
  },

  getLastMode: function()
  {
    return _store.lastMode;
  },

  getView: function()
  {
    return _store.view;
  },

  getLastView: function()
  {
    return _store.lastView;
  }

};

/*! @licend */

},{"../utilities/connectable":41}],37:[function(require,module,exports){
/*!
 * @source /js/models/tabs.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _connectable = require('../utilities/connectable');

var _isGhostMode = require('../helpers/isGhostMode');

var _store = {
  'normal-1': {
    id: 'normal-1',
    ghost: false,
    page: 'normalhome',
    history: ['normalhome'],
    historyIndex: 0
  },
  'ghost-1': {
    id: 'ghost-1',
    ghost: true,
    page: 'ghosthome',
    history: ['ghosthome'],
    historyIndex: 0
  }
};

var _normalCount = 1;
var _ghostCount = 1;
var _normalActive = 'normal-1';
var _ghostActive = 'ghost-1';
var _normalLength = 1;
var _ghostLength = 1;
var _normalHistory = ['normal-1'];
var _ghostHistory = ['ghost-1'];

module.exports = {

  create: _connectable(function(ghost)
  {
    if (ghost === undefined)
    {
      ghost = _isGhostMode();
    }

    var id;

    if (ghost)
    {
      _ghostLength++;
      _ghostCount++;
      id = 'ghost-' + _ghostCount;
    }
    else
    {
      _normalLength++;
      _normalCount++;
      id = 'normal-' + _normalCount;
    }

    return _store[id] = {
      id: id,
      ghost: ghost,
      page: ghost ? 'ghosthome' : 'normalhome',
      history: [ghost ? 'ghosthome': 'normalhome'],
      historyIndex: 0
    };
  }),

  destroy: _connectable(function(tab)
  {
    tab.ghost ? _ghostLength-- : _normalLength--;
    delete _store[tab.id];
    return tab;
  }),

  switch: _connectable(function(tab)
  {
    if (tab.ghost)
    {
      _ghostActive = tab.id;
      _ghostHistory.push(tab.id);
    }
    else
    {
      _normalActive = tab.id;
      _normalHistory.push(tab.id);
    }
    return tab;
  }),

  setPage: _connectable(function(tab, page)
  {
    tab.page = page.id;
    tab.history.push(page.id);
    tab.historyIndex = tab.history.length - 1;
    return tab;
  }),

  goBack: _connectable(function(tab)
  {
    if (tab.history.length > 0)
    {
      tab.historyIndex--;
      tab.page = tab.history[tab.historyIndex];
    }
    return tab;
  }),

  goForward: _connectable(function(tab)
  {
    if (tab.historyIndex < tab.history.length - 1)
    {
      tab.historyIndex++;
      tab.page = tab.history[tab.historyIndex];
    }
    return tab;
  }),

  get: function(id)
  {
    return _store[id];
  },

  getActive: function(ghost)
  {
    if (ghost === undefined)
    {
      ghost = _isGhostMode();
    }

    return _store[ghost ? _ghostActive : _normalActive];
  },

  getMostRecent: function(ghost)
  {
    if (ghost === undefined)
    {
      ghost = _isGhostMode();
    }

    var active = ghost ? _ghostActive : _normalActive;
    var history = ghost ? _ghostHistory : _normalHistory;

    if (_store[active])
    {
      return _store[active];
    }

    for (var tab = history.length - 1; tab > -1; tab--)
    {
      if (_store[history[tab]])
      {
        return _store[history[tab]];
      }
    }
  },

  getLength: function(ghost)
  {
    if (ghost === undefined)
    {
      ghost = _isGhostMode();
    }

    return ghost ? _ghostLength : _normalLength;
  }

};

/*! @licend */

},{"../helpers/isGhostMode":28,"../utilities/connectable":41}],38:[function(require,module,exports){
/*!
 * @source /js/models/whitelist.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _connectable = require('../utilities/connectable');

var _store = {};

var model = {

  create: _connectable(function(page)
  {
    return _store[page.id] = page;
  }),

  destroy: _connectable(function(page)
  {
    delete _store[page.id];
    return page;
  }),

  get: function(page)
  {
    return _store[page.id];
  }

};

module.exports = model;

/*! @licend */

},{"../utilities/connectable":41}],39:[function(require,module,exports){
/*!
 * @source /js/util/MobileButtonList.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software = you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http = //www.gnu.org/licenses/>.
 */

var Sortable = require('../../node_modules/sortablejs/Sortable');

var START_THRESHOLD = 5;
var DRAG_THRESHOLD = 0.35;

var delegate = require('./delegate');

function MobileButtonList(options)
{
  var list = this._list = options.list;
  var selector = this._selector = options.selector;
  this.onSelect = options.onSelect;
  this.onDestroy = options.onDestroy;
  this.onReorder = options.onReorder;
  this._dragTimer = null;
  this._isTouching = false;
  this._isDragging = false;
  this._dragStartX = 0;
  this._dragEndX = 0;
  this._dragThreshold = 0;
  this._dragButton = null;
  delegate(list, selector, 'touchstart', this._onTouchStart.bind(this));
  list.addEventListener('touchmove', this._onTouchMove.bind(this), false);
  document.addEventListener('touchend', this._onTouchEnd.bind(this), false);
  this.sortable = new Sortable(list, {
    delay: 200,
    filter: '.swiping',
    dropBubble: true,
    dragoverBubble: true,
    onStart: function()
    {
      this._isSorting = true;
    }.bind(this)
  });
}

MobileButtonList.prototype = {

  _normalizeEvent: function(event)
  {
    if (event.touches && event.touches.length)
    {
      return event.touches[0];
    }
    else
    {
      return event;
    }
  },

  _onTouchStart: function(target, event)
  {
    this._isTouching = true;
    this._dragButton = target;
    if (this._dragTimer)
    {
      clearTimeout(this._dragTimer);
    }
    this._dragTimer = setTimeout(
      this._onDragStart.bind(this, target, this._normalizeEvent(event)),
      150
    );
  },

  _onDragStart: function(target, event)
  {
    if (this._isDragging === false && Math.abs(event.clientX - this._dragEndX) > START_THRESHOLD)
    {
      this._dragStartX = event.clientX;
      this._dragStartY = event.clientY;
      this._dragThreshold = target.offsetWidth * DRAG_THRESHOLD;
      this._dragButton.classList.add('swiping');
      this._isDragging = true;
    }
  },

  _onTouchMove: function(event)
  {
    event = this._normalizeEvent(event);
    this._dragEndX = event.clientX;
    if (this._isDragging && !this._isSorting)
    {
      var dragDistance = this._dragStartX - this._dragEndX;
      if (dragDistance > 0)
      {
        this._dragButton.style.marginLeft = (dragDistance * -1) + 'px';
      }
    }
  },

  _onTouchEnd: function()
  {
    if (this._isDragging)
    {
      this._isDragging = false;
      this._isTouching = false;

      if (this._dragStartX - this._dragEndX >= this._dragThreshold)
      {
        this.onDestroy(this._dragButton);
      }
      else
      {
        this._dragButton.style.marginLeft = '0px';
      }

      this._dragButton.removeAttribute('draggable');
    }
    else if (this._isTouching)
    {
      this._isTouching = false;
      clearTimeout(this._dragTimer);

      if (this._isSorting === false)
      {
        this.onSelect(this._dragButton);
      }
    }
    if (this._dragButton)
    {
      this._dragButton.classList.remove('swiping');
    }
    this._isSorting = false;
    this._isDragging = false;
    this._dragButton = null;
    this._dragStartX = 0;
    this._dragEndX = 0;
  }

};

module.exports = MobileButtonList;

/*! @licend */

},{"../../node_modules/sortablejs/Sortable":53,"./delegate":42}],40:[function(require,module,exports){
/*!
 * @source /js/utilities/animate.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function animate(target, animation)
{
  var lastAnimation = target.getAttribute('last-animation');
  var onAnimationEnd = function()
  {
    target.removeEventListener('animationend', onAnimationEnd);
    target.classList.remove('animating');
  };
  target.classList.remove(lastAnimation);
  target.setAttribute('last-animation', animation);
  target.addEventListener('animationend', onAnimationEnd);
  target.classList.add('animated', 'animating', animation);
};

/*! @licend */

},{}],41:[function(require,module,exports){
/*!
 * @source /js/utilities/connectable.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

function returnPayload(payload)
{
  return payload;
}

function connectable(callback)
{
  if (typeof callback !== 'function')
  {
    callback = returnPayload;
  }

  var connections = {};
  var index = 0;

  var emit = function()
  {
    var payload = callback.apply(this, Array.prototype.slice.call(arguments));
    for (var connection in connections)
    {
      connections[connection](payload);
    }
    return payload;
  };

  emit.connect = function(callback)
  {
    if (typeof callback !== 'function') throw new TypeError();
    var id = 'CALLBACK_' + index++;
    connections[id] = callback;
    return id;
  };

  emit.disconnect = function(id)
  {
    delete connections[id];
  };

  return emit;
}

module.exports = connectable;

/*! @licend */

},{}],42:[function(require,module,exports){
/*!
 * @source /js/utilities/delegate.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

function getParentWithClassName(limit, child, className)
{
  while (!child.classList.contains(className) && child !== limit)
  {
    child = child.parentNode;
  }
  if (child === limit)
  {
    return undefined;
  }
  else
  {
    return child;
  }
}

module.exports = function delegate(parent, child, event, callback)
{
  parent.addEventListener(event, function(event)
  {
    var target = getParentWithClassName(parent, event.target, child);
    if (target)
    {
      callback.call(target, target, event);
    }
  }, true);
};

/*! @licend */

},{}],43:[function(require,module,exports){
/*!
 * @source /js/utilities/render.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function render(template, values)
{
  var parent = document.createElement('div');
  if (values)
  {
    for (var key in values)
    {
      //FIXME: THIS IS NOT SAFE
      template = template.replace(new RegExp('{{' + key + '}}', 'g'), values[key]);
    }
  }
  //FIXME: THIS IS NOT SAFE
  parent.innerHTML = template;
  return parent.children.length > 1 ? parent.children : parent.children[0];
};

/*! @licend */

},{}],44:[function(require,module,exports){
/*!
 * @source /js/views/location.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _render = require('../utilities/render');
var _connectable = require('../utilities/connectable');

var _template = require('../../html/addressbar.html');

var _addressbar = _render(_template);
var _location = _addressbar.querySelector('#location');
var _favoriteButton = _addressbar.querySelector('#addressbar-favorite');
var _optionsButton = _addressbar.querySelector('#addressbar-options');
var _cancelButton = _addressbar.querySelector('#addressbar-cancel');

var view = {

  el: _addressbar,

  getLocation: function()
  {
    return _location.value;
  },

  setLocation: function(value)
  {
    _location.value = value;
  },

  blur: function()
  {
    _location.blur();
  },

  selectLocation: function()
  {
    _addressbar.classList.add('selected');
    _location.select();
  },

  deselectLocation: function()
  {
    _addressbar.classList.remove('selected');
    window.getSelection().removeAllRanges();
  },

  enable: function(internal)
  {
    _addressbar.classList.remove('disabled');
    _location.disabled = false;
    if (!internal)
    {
      _favoriteButton.disabled = false;
    }
  },

  disable: function()
  {
    _addressbar.classList.add('disabled');
    _location.disabled = true;
    _favoriteButton.disabled = true;
    _location.blur();
  },

  enableFavorite: function()
  {
    _favoriteButton.disabled = false;
  },

  disableFavorite: function()
  {
    _favoriteButton.disabled = true;
  },

  favorite: function()
  {
    _favoriteButton.children[0].textContent = 'favorite_filled';
  },

  unfavorite: function()
  {
    _favoriteButton.children[0].textContent = 'favorite';
  },

  onBlur: _connectable(),

  onFocus: _connectable(),

  onSubmit: _connectable(),

  onChange: _connectable(),

  onCancel: _connectable(),

  onToggleOptions: _connectable(),

  onToggleFavorite: _connectable()

};

_addressbar.addEventListener('submit', function(event)
{
  event.preventDefault();
  view.onSubmit(view.getLocation());
});

_cancelButton.addEventListener('click', view.onCancel);
_location.addEventListener('blur', view.onBlur);
_location.addEventListener('focus', view.onFocus);
_optionsButton.addEventListener('click', view.onToggleOptions);
_favoriteButton.addEventListener('click', view.onToggleFavorite);

module.exports = view;

/*! @licend */

},{"../../html/addressbar.html":1,"../utilities/connectable":41,"../utilities/render":43}],45:[function(require,module,exports){
/*!
 * @source /js/views/bookmarks.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _render = require('../utilities/render');
var _animate = require('../utilities/animate');
var _connectable = require('../utilities/connectable');
var _MobileButtonList = require('../utilities/MobileButtonList');

var _modalTemplate = require('../../html/bookmarks.html');
var _itemTemplate = require('../../html/bookmark.html');

var _modal = _render(_modalTemplate);

var _list = _modal.querySelector('#bookmarks-list');

var view = {

  el: _modal,

  open: function()
  {
    _animate(_modal, 'slideInRight');
  },

  close: function()
  {
    _animate(_modal, 'slideOutRight');
  },

  create: function(id, title, location)
  {
    var child = _render(
      _itemTemplate, {
        id: id,
        title: title,
        location: location
      }
    );

    _list.appendChild(child);
  },

  destroy: function(id)
  {
    var child = document.getElementById(id);

    if (child)
    {
      _list.removeChild(child);
    }
  },

  onDestroy: _connectable(),

  onSelect: _connectable(),

  onReorder: _connectable()

};

var _onSelect = function(button)
{
  view.onSelect(button.parentNode.id);
};

var _onDestroy = function(button)
{
  button.addEventListener(
    'animationend',
    _onDestroyComplete.bind(this, button)
  );
  _animate(button, 'slideOutLeft');
};

var _onDestroyComplete = function(button)
{
  button.removeEventListener('animationend', _onDestroyComplete);
  view.onDestroy(button.parentNode.id);
};

view.buttonList = new _MobileButtonList({
  list: _list,
  selector: 'bookmark-button',
  onSelect: _onSelect,
  onDestroy: _onDestroy
});

module.exports = view;

/*! @licend */

},{"../../html/bookmark.html":2,"../../html/bookmarks.html":3,"../utilities/MobileButtonList":39,"../utilities/animate":40,"../utilities/connectable":41,"../utilities/render":43}],46:[function(require,module,exports){
/*!
 * @source /js/views/container.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _render = require('../utilities/render');

var _template = require('../../html/container.html');

var _container = _render(_template);

module.exports = {

  el: _container,

  setMode: function(mode, lastMode)
  {
    _container.classList.remove(lastMode);
    _container.classList.add(mode);
  },

  setView: function(view, lastView)
  {
    _container.classList.remove(lastView);
    _container.classList.add(view);
  },

  setWhitelisted: function(whitelisted)
  {
    if (whitelisted)
    {
      _container.classList.add('whitelisted');
    }
    else
    {
      _container.classList.remove('whitelisted');
    }
  },

  setBookmarked: function(bookmarked)
  {
    if (bookmarked)
    {
      _container.classList.add('bookmarked');
    }
    else
    {
      _container.classList.remove('bookmarked');
    }
  }

};

/*! @licend */

},{"../../html/container.html":4,"../utilities/render":43}],47:[function(require,module,exports){
/*!
 * @source /js/views/history.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _render = require('../utilities/render');
var _animate = require('../utilities/animate');
var _delegate = require('../utilities/delegate');
var _connectable = require('../utilities/connectable');
var _MobileButtonList = require('../utilities/MobileButtonList');

var _modalTemplate = require('../../html/history.html');
var _itemTemplate = require('../../html/historyitem.html');

var _modal = _render(_modalTemplate);

var _list = _modal.querySelector('#history-list');
var _backButton = _modal.querySelector('#history-toggle');
var _deleteButton = _modal.querySelector('#history-delete');

var WEEK_DAYS = [
  'SUNDAY',
  'MONDAY',
  'TUESDAY',
  'WEDNESDAY',
  'THURSDAY',
  'FRIDAY',
  'SATURDAY'
];

var MONTHS = [
  'JAN',
  'FEB',
  'MAR',
  'APR',
  'MAY',
  'JUN',
  'JUL',
  'AUG',
  'SEP',
  'OCT',
  'NOV',
  'DEC'
];

var _today = document.createElement('li');
_today.classList.add('history-date');
var now = new Date();
_today.textContent = WEEK_DAYS[now.getDay()] + ', ' +
  MONTHS[now.getMonth()] + ', ' + now.getFullYear();

_list.appendChild(_today);

var view = {

  el: _modal,

  open: function()
  {
    _animate(_modal, 'slideInRight');
  },

  close: function()
  {
    _animate(_modal, 'slideOutRight');
  },

  create: function(id, title, location)
  {
    var child = _render(
      _itemTemplate, {
        id: id,
        title: title,
        location: location
      }
    );

    _list.appendChild(child);
  },

  destroy: function(id)
  {
    var child = document.getElementById(id);

    if (child)
    {
      _list.removeChild(child);
    }
  },

  setBookmark: function(location, active)
  {
    var children = _list.querySelectorAll('li[data-location="' + location + '"]');

    if (!children)
    {
      return;
    }

    for (var i = 0; i < children.length; i++)
    {
      children[i]['classList'][active ? 'add': 'remove']('bookmarked');
    }
  },

  onDelete: _connectable(),

  onBack: _connectable(),

  onDestroy: _connectable(),

  onSelect: _connectable(),

  onReorder: _connectable(),

  onToggleBookmark: _connectable()

};

_delegate(_list, 'history-bookmark', 'click', function(target, event)
{
  event.preventDefault();
  event.stopPropagation();
  view.onToggleBookmark(target.id);
});

_backButton.addEventListener('click', view.onBack);
_deleteButton.addEventListener('click', view.onDelete);

var _onSelect = function(button)
{
  view.onSelect(button.parentNode.id);
};

var _onDestroy = function(button)
{
  button.addEventListener(
    'animationend',
    _onDestroyComplete.bind(this, button)
  );
  _animate(button, 'slideOutLeft');
};

var _onDestroyComplete = function(button)
{
  button.removeEventListener('animationend', _onDestroyComplete);
  view.onDestroy(button.parentNode.id);
};

view.buttonList = new _MobileButtonList({
  list: _list,
  selector: 'history-button',
  onSelect: _onSelect,
  onDestroy: _onDestroy
});

module.exports = view;

/*! @licend */

},{"../../html/history.html":7,"../../html/historyitem.html":8,"../utilities/MobileButtonList":39,"../utilities/animate":40,"../utilities/connectable":41,"../utilities/delegate":42,"../utilities/render":43}],48:[function(require,module,exports){
/*!
 * @source /js/views/options.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _render = require('../utilities/render');
var _animate = require('../utilities/animate');
var _connectable = require('../utilities/connectable');

var _template = require('../../html/options.html');

var _options = _render(_template);
var _refreshButton = _options.querySelector('#options-refresh');
var _adblockButton = _options.querySelector('#options-adblock');
var _favoriteButton = _options.querySelector('#options-bookmark');
var _shareButton = _options.querySelector('#options-share');
var _historyButton = _options.querySelector('#options-history');

var view = {

  el: _options,

  open: function()
  {
    _animate(_options, 'slideInDown');
  },

  close: function()
  {
    _animate(_options, 'slideOutUp');
  },

  favorite: function()
  {
    _favoriteButton.querySelector('i').textContent = 'favorite_filled';
    _favoriteButton.querySelector('span').textContent = 'Page bookmarked';
  },

  unfavorite: function()
  {
    _favoriteButton.querySelector('i').textContent = 'favorite';
    _favoriteButton.querySelector('span').textContent = 'Bookmark page';
  },

  update: function(page)
  {
    _refreshButton.disabled = page.internal;
    _adblockButton.disabled = page.internal;
    _favoriteButton.disabled = page.internal;
    _shareButton.disabled = page.internal;
  },

  onRefreshTab: _connectable(),

  onToggleAdblock: _connectable(),

  onToggleFavorite: _connectable(),

  onToggleHistory: _connectable(),

  onBlur: _connectable()

};

_refreshButton.addEventListener('click', view.onRefreshTab);
_adblockButton.addEventListener('click', view.onToggleAdblock);
_favoriteButton.addEventListener('click', view.onToggleFavorite);
_historyButton.addEventListener('click', view.onToggleHistory);
document.addEventListener('click', view.onBlur);

module.exports = view;

/*! @licend */

},{"../../html/options.html":10,"../utilities/animate":40,"../utilities/connectable":41,"../utilities/render":43}],49:[function(require,module,exports){
/*!
 * @source /js/views/pages.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _render = require('../utilities/render');
var _connectable = require('../utilities/connectable');

var _template = require('../../html/pages.html');

var _templates = {
  'normalhome': require('../../html/normalhome.html'),
  'ghosthome': require('../../html/ghosthome.html'),
  'examplepage': require('../../html/exampleiframe.html')
};

var _container = _render(_template);

var _onClick = function(container, event)
{
  event.preventDefault();

  var target = event.target;

  while (target !== container && target.nodeName !== 'A')
  {
    target = target.parentNode;
  }

  var href = target.getAttribute('href');

  if (href)
  {
    view.onNavigate(href);
  }
  else
  {
    view.onBlur();
  }
};

_container.addEventListener('click', _onClick.bind(_container, _container));

var view = {

  el: _container,

  create: function(id, title, template, whitelisted)
  {
    var child = _render(_templates[template], {
      id: id,
      title: title,
      showAds: !whitelisted
    });

    if (child.nodeName === 'IFRAME')
    {
      child.contentDocument.addEventListener('click', _onClick.bind(child, child));
    }

    _container.appendChild(child);
  },

  destroy: function(id)
  {
    var child = _container.querySelector('.' + id);

    if (child)
    {
      child.parentNode.removeChild(child);
    }
  },

  switch: function(id)
  {
    var child = _container.querySelector('.active');

    if (child)
    {
      child.classList.remove('active');
    }

    child = _container.querySelector('.' + id);

    if (child)
    {
      child.classList.add('active');
    }
  },

  change: function(id, title, template, whitelisted)
  {
    var child = _container.querySelector('.' + id);

    if (child)
    {
      child.parentNode.removeChild(child);

      child = _render(_templates[template], {
        id: id,
        title: title,
        showAds: whitelisted ? 1 : 0
      });

      child.classList.add('active');

      _container.appendChild(child);
    }
  },

  onNavigate: _connectable(),

  onBlur: _connectable()

};

module.exports = view;

/*! @licend */

},{"../../html/exampleiframe.html":5,"../../html/ghosthome.html":6,"../../html/normalhome.html":9,"../../html/pages.html":11,"../utilities/connectable":41,"../utilities/render":43}],50:[function(require,module,exports){
/*!
 * @source /js/views/splashscreen.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _animate = require('../utilities/animate');

module.exports = {

  destroy: function destroy()
  {
    var splashscreen = document.getElementById('splashscreen');
    var onAnimationEnd = function()
    {
      splashscreen.removeEventListener('animationend', onAnimationEnd);
      splashscreen.parentNode.removeChild(splashscreen);
      splashscreen = null;
    };
    splashscreen.addEventListener('animationend', onAnimationEnd);
    _animate(splashscreen, 'fadeOut');
  }

};

/*! @licend */

},{"../utilities/animate":40}],51:[function(require,module,exports){
/*!
 * @source /js/views/tabs.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _render = require('../utilities/render');
var _animate = require('../utilities/animate');
var _connectable = require('../utilities/connectable');
var _MobileButtonList = require('../utilities/MobileButtonList');

var _modalTemplate = require('../../html/tabs.html');
var _tabTemplate = require('../../html/tab.html');

var _normalModal = _render(_modalTemplate, {mode: 'normal'});
var _ghostModal = _render(_modalTemplate, {mode: 'ghost'});
var _normalList = _normalModal.querySelector('.tabs-list');
var _ghostList = _ghostModal.querySelector('.tabs-list');
var _normalCreate = _normalModal.querySelector('.tabs-create');
var _ghostCreate = _ghostModal.querySelector('.tabs-create');

var view = {

  normal: _normalModal,

  ghost: _ghostModal,

  open: function(ghost)
  {
    _animate(ghost ? _ghostModal : _normalModal, 'slideInUp');
  },

  close: function(ghost)
  {
    _animate(ghost ? _ghostModal : _normalModal, 'slideOutDown');
  },

  updateTab: function(tab, page)
  {
    var child = document.getElementById(tab.id);
    child.querySelector('.tab-title').textContent = page.title;
    child.querySelector('.tab-url').textContent = page.location;
  },

  createTab: function(tab, page)
  {
    var list = tab.ghost ? _ghostList : _normalList;

    var child = _render(
      _tabTemplate, {
        id: tab.id,
        title: page.title,
        location: page.location
      }
    );

    list.appendChild(child);
  },

  destroyTab: function(tab)
  {
    var list = tab.ghost ? _ghostList : _normalList;
    var child = document.getElementById(tab.id);

    if (child)
    {
      list.removeChild(child);
    }
  },

  onCreate: _connectable(),

  onDestroy: _connectable(),

  onSelect: _connectable(),

  onResort: _connectable()

};

_normalCreate.addEventListener('click', view.onCreate.bind(view, false));
_ghostCreate.addEventListener('click', view.onCreate.bind(view, true));

var _onSelect = function(button)
{
  view.onSelect(button.parentNode.id);
};

var _onDestroy = function(button)
{
  button.addEventListener(
    'animationend',
    _onDestroyComplete.bind(this, button)
  );
  _animate(button, 'slideOutLeft');
};

var _onDestroyComplete = function(button)
{
  button.removeEventListener('animationend', _onDestroyComplete);
  view.onDestroy(button.parentNode.id);
};

view.normalButtonList = new _MobileButtonList({
  list: _normalList,
  selector: 'tab-button',
  onSelect: _onSelect,
  onDestroy: _onDestroy
});

view.ghostButtonList = new _MobileButtonList({
  list: _ghostList,
  selector: 'tab-button',
  onSelect: _onSelect,
  onDestroy: _onDestroy
});

module.exports = view;

/*! @licend */

},{"../../html/tab.html":12,"../../html/tabs.html":13,"../utilities/MobileButtonList":39,"../utilities/animate":40,"../utilities/connectable":41,"../utilities/render":43}],52:[function(require,module,exports){
/*!
 * @source /js/views/tapbar.js
 * @licstart This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

var _render = require('../utilities/render');
var _connectable = require('../utilities/connectable');

var _template = require('../../html/tapbar.html');

var _tapbar = _render(_template);

var _normalTabsButton = _tapbar.querySelector('#tapbar-normal-tabs');
var _ghostTabsButton = _tapbar.querySelector('#tapbar-ghost-tabs');
var _bookmarkButton = _tapbar.querySelector('#tapbar-bookmarks');
var _backButton = _tapbar.querySelector('#tapbar-back');
var _forwardButton = _tapbar.querySelector('#tapbar-forward');
var _ghostTabsIcon = _ghostTabsButton.querySelector('i');
var _ghostTabsCount = _ghostTabsButton.querySelector('span');
var _normalTabsIcon = _normalTabsButton.querySelector('i');
var _normalTabsCount = _normalTabsButton.querySelector('span');
var _bookmarkIcon = _bookmarkButton.querySelector('i');

var view = {

  el: _tapbar,

  selectNormalTabs: function()
  {
    _normalTabsIcon.textContent = 'tabs_filled';
    _bookmarkIcon.textContent = 'bookmark';
    _ghostTabsIcon.textContent = 'ghost';
  },

  selectGhostTabs: function()
  {
    _normalTabsIcon.textContent = 'tabs';
    _bookmarkIcon.textContent = 'bookmark';
    _ghostTabsIcon.textContent = 'ghost_filled';
  },

  selectBookmarks: function()
  {
    _normalTabsIcon.textContent = 'tabs';
    _bookmarkIcon.textContent = 'bookmark_filled';
    _ghostTabsIcon.textContent = 'ghost';
  },

  setGhostTabsLength: function(length)
  {
    if (length < 1)
    {
      length = '';
    }
    _ghostTabsCount.textContent = length;
  },

  setNormalTabsLength: function(length)
  {
    if (length < 1)
    {
      length = '';
    }
    _normalTabsCount.textContent = length;
  },

  enableBack: function()
  {
    _backButton.disabled = false;
  },

  disableBack: function()
  {
    _backButton.disabled = true;
  },

  enableForward: function()
  {
    _forwardButton.disabled = false;
  },

  disableForward: function()
  {
    _forwardButton.disabled = true;
  },

  onForward: _connectable(),

  onBack: _connectable(),

  onToggleNormalTabs: _connectable(),

  onToggleGhostTabs: _connectable(),

  onToggleBookmarks: _connectable()

};

_backButton.addEventListener('click', view.onBack);
_forwardButton.addEventListener('click', view.onForward);
_normalTabsButton.addEventListener('click', view.onToggleNormalTabs);
_ghostTabsButton.addEventListener('click', view.onToggleGhostTabs);
_bookmarkButton.addEventListener('click', view.onToggleBookmarks);

module.exports = view;

/*! @licend */

},{"../../html/tapbar.html":14,"../utilities/connectable":41,"../utilities/render":43}],53:[function(require,module,exports){
/**!
 * Sortable
 * @author	RubaXa   <trash@rubaxa.org>
 * @license MIT
 */


(function (factory) {
	"use strict";

	if (typeof define === "function" && define.amd) {
		define(factory);
	}
	else if (typeof module != "undefined" && typeof module.exports != "undefined") {
		module.exports = factory();
	}
	else if (typeof Package !== "undefined") {
		Sortable = factory();  // export for Meteor.js
	}
	else {
		/* jshint sub:true */
		window["Sortable"] = factory();
	}
})(function () {
	"use strict";

	if (typeof window == "undefined" || typeof window.document == "undefined") {
		return function() {
			throw new Error( "Sortable.js requires a window with a document" );
		}
	}

	var dragEl,
		parentEl,
		ghostEl,
		cloneEl,
		rootEl,
		nextEl,

		scrollEl,
		scrollParentEl,

		lastEl,
		lastCSS,
		lastParentCSS,

		oldIndex,
		newIndex,

		activeGroup,
		autoScroll = {},

		tapEvt,
		touchEvt,

		moved,

		/** @const */
		RSPACE = /\s+/g,

		expando = 'Sortable' + (new Date).getTime(),

		win = window,
		document = win.document,
		parseInt = win.parseInt,

		supportDraggable = !!('draggable' in document.createElement('div')),
		supportCssPointerEvents = (function (el) {
			el = document.createElement('x');
			el.style.cssText = 'pointer-events:auto';
			return el.style.pointerEvents === 'auto';
		})(),

		_silent = false,

		abs = Math.abs,
		slice = [].slice,

		touchDragOverListeners = [],

		_autoScroll = _throttle(function (/**Event*/evt, /**Object*/options, /**HTMLElement*/rootEl) {
			// Bug: https://bugzilla.mozilla.org/show_bug.cgi?id=505521
			if (rootEl && options.scroll) {
				var el,
					rect,
					sens = options.scrollSensitivity,
					speed = options.scrollSpeed,

					x = evt.clientX,
					y = evt.clientY,

					winWidth = window.innerWidth,
					winHeight = window.innerHeight,

					vx,
					vy
				;

				// Delect scrollEl
				if (scrollParentEl !== rootEl) {
					scrollEl = options.scroll;
					scrollParentEl = rootEl;

					if (scrollEl === true) {
						scrollEl = rootEl;

						do {
							if ((scrollEl.offsetWidth < scrollEl.scrollWidth) ||
								(scrollEl.offsetHeight < scrollEl.scrollHeight)
							) {
								break;
							}
							/* jshint boss:true */
						} while (scrollEl = scrollEl.parentNode);
					}
				}

				if (scrollEl) {
					el = scrollEl;
					rect = scrollEl.getBoundingClientRect();
					vx = (abs(rect.right - x) <= sens) - (abs(rect.left - x) <= sens);
					vy = (abs(rect.bottom - y) <= sens) - (abs(rect.top - y) <= sens);
				}


				if (!(vx || vy)) {
					vx = (winWidth - x <= sens) - (x <= sens);
					vy = (winHeight - y <= sens) - (y <= sens);

					/* jshint expr:true */
					(vx || vy) && (el = win);
				}


				if (autoScroll.vx !== vx || autoScroll.vy !== vy || autoScroll.el !== el) {
					autoScroll.el = el;
					autoScroll.vx = vx;
					autoScroll.vy = vy;

					clearInterval(autoScroll.pid);

					if (el) {
						autoScroll.pid = setInterval(function () {
							if (el === win) {
								win.scrollTo(win.pageXOffset + vx * speed, win.pageYOffset + vy * speed);
							} else {
								vy && (el.scrollTop += vy * speed);
								vx && (el.scrollLeft += vx * speed);
							}
						}, 24);
					}
				}
			}
		}, 30),

		_prepareGroup = function (options) {
			var group = options.group;

			if (!group || typeof group != 'object') {
				group = options.group = {name: group};
			}

			['pull', 'put'].forEach(function (key) {
				if (!(key in group)) {
					group[key] = true;
				}
			});

			options.groups = ' ' + group.name + (group.put.join ? ' ' + group.put.join(' ') : '') + ' ';
		}
	;



	/**
	 * @class  Sortable
	 * @param  {HTMLElement}  el
	 * @param  {Object}       [options]
	 */
	function Sortable(el, options) {
		if (!(el && el.nodeType && el.nodeType === 1)) {
			throw 'Sortable: `el` must be HTMLElement, and not ' + {}.toString.call(el);
		}

		this.el = el; // root element
		this.options = options = _extend({}, options);


		// Export instance
		el[expando] = this;


		// Default options
		var defaults = {
			group: Math.random(),
			sort: true,
			disabled: false,
			store: null,
			handle: null,
			scroll: true,
			scrollSensitivity: 30,
			scrollSpeed: 10,
			draggable: /[uo]l/i.test(el.nodeName) ? 'li' : '>*',
			ghostClass: 'sortable-ghost',
			chosenClass: 'sortable-chosen',
			ignore: 'a, img',
			filter: null,
			animation: 0,
			setData: function (dataTransfer, dragEl) {
				dataTransfer.setData('Text', dragEl.textContent);
			},
			dropBubble: false,
			dragoverBubble: false,
			dataIdAttr: 'data-id',
			delay: 0,
			forceFallback: false,
			fallbackClass: 'sortable-fallback',
			fallbackOnBody: false
		};


		// Set default options
		for (var name in defaults) {
			!(name in options) && (options[name] = defaults[name]);
		}

		_prepareGroup(options);

		// Bind all private methods
		for (var fn in this) {
			if (fn.charAt(0) === '_') {
				this[fn] = this[fn].bind(this);
			}
		}

		// Setup drag mode
		this.nativeDraggable = options.forceFallback ? false : supportDraggable;

		// Bind events
		_on(el, 'mousedown', this._onTapStart);
		_on(el, 'touchstart', this._onTapStart);

		if (this.nativeDraggable) {
			_on(el, 'dragover', this);
			_on(el, 'dragenter', this);
		}

		touchDragOverListeners.push(this._onDragOver);

		// Restore sorting
		options.store && this.sort(options.store.get(this));
	}


	Sortable.prototype = /** @lends Sortable.prototype */ {
		constructor: Sortable,

		_onTapStart: function (/** Event|TouchEvent */evt) {
			var _this = this,
				el = this.el,
				options = this.options,
				type = evt.type,
				touch = evt.touches && evt.touches[0],
				target = (touch || evt).target,
				originalTarget = target,
				filter = options.filter;


			if (type === 'mousedown' && evt.button !== 0 || options.disabled) {
				return; // only left button or enabled
			}

			target = _closest(target, options.draggable, el);

			if (!target) {
				return;
			}

			// get the index of the dragged element within its parent
			oldIndex = _index(target, options.draggable);

			// Check filter
			if (typeof filter === 'function') {
				if (filter.call(this, evt, target, this)) {
					_dispatchEvent(_this, originalTarget, 'filter', target, el, oldIndex);
					evt.preventDefault();
					return; // cancel dnd
				}
			}
			else if (filter) {
				filter = filter.split(',').some(function (criteria) {
					criteria = _closest(originalTarget, criteria.trim(), el);

					if (criteria) {
						_dispatchEvent(_this, criteria, 'filter', target, el, oldIndex);
						return true;
					}
				});

				if (filter) {
					evt.preventDefault();
					return; // cancel dnd
				}
			}


			if (options.handle && !_closest(originalTarget, options.handle, el)) {
				return;
			}


			// Prepare `dragstart`
			this._prepareDragStart(evt, touch, target);
		},

		_prepareDragStart: function (/** Event */evt, /** Touch */touch, /** HTMLElement */target) {
			var _this = this,
				el = _this.el,
				options = _this.options,
				ownerDocument = el.ownerDocument,
				dragStartFn;

			if (target && !dragEl && (target.parentNode === el)) {
				tapEvt = evt;

				rootEl = el;
				dragEl = target;
				parentEl = dragEl.parentNode;
				nextEl = dragEl.nextSibling;
				activeGroup = options.group;

				dragStartFn = function () {
					// Delayed drag has been triggered
					// we can re-enable the events: touchmove/mousemove
					_this._disableDelayedDrag();

					// Make the element draggable
					dragEl.draggable = true;

					// Chosen item
					_toggleClass(dragEl, _this.options.chosenClass, true);

					// Bind the events: dragstart/dragend
					_this._triggerDragStart(touch);
				};

				// Disable "draggable"
				options.ignore.split(',').forEach(function (criteria) {
					_find(dragEl, criteria.trim(), _disableDraggable);
				});

				_on(ownerDocument, 'mouseup', _this._onDrop);
				_on(ownerDocument, 'touchend', _this._onDrop);
				_on(ownerDocument, 'touchcancel', _this._onDrop);

				if (options.delay) {
					// If the user moves the pointer or let go the click or touch
					// before the delay has been reached:
					// disable the delayed drag
					_on(ownerDocument, 'mouseup', _this._disableDelayedDrag);
					_on(ownerDocument, 'touchend', _this._disableDelayedDrag);
					_on(ownerDocument, 'touchcancel', _this._disableDelayedDrag);
					_on(ownerDocument, 'mousemove', _this._disableDelayedDrag);
					_on(ownerDocument, 'touchmove', _this._disableDelayedDrag);

					_this._dragStartTimer = setTimeout(dragStartFn, options.delay);
				} else {
					dragStartFn();
				}
			}
		},

		_disableDelayedDrag: function () {
			var ownerDocument = this.el.ownerDocument;

			clearTimeout(this._dragStartTimer);
			_off(ownerDocument, 'mouseup', this._disableDelayedDrag);
			_off(ownerDocument, 'touchend', this._disableDelayedDrag);
			_off(ownerDocument, 'touchcancel', this._disableDelayedDrag);
			_off(ownerDocument, 'mousemove', this._disableDelayedDrag);
			_off(ownerDocument, 'touchmove', this._disableDelayedDrag);
		},

		_triggerDragStart: function (/** Touch */touch) {
			if (touch) {
				// Touch device support
				tapEvt = {
					target: dragEl,
					clientX: touch.clientX,
					clientY: touch.clientY
				};

				this._onDragStart(tapEvt, 'touch');
			}
			else if (!this.nativeDraggable) {
				this._onDragStart(tapEvt, true);
			}
			else {
				_on(dragEl, 'dragend', this);
				_on(rootEl, 'dragstart', this._onDragStart);
			}

			try {
				if (document.selection) {
					document.selection.empty();
				} else {
					window.getSelection().removeAllRanges();
				}
			} catch (err) {
			}
		},

		_dragStarted: function () {
			if (rootEl && dragEl) {
				// Apply effect
				_toggleClass(dragEl, this.options.ghostClass, true);

				Sortable.active = this;

				// Drag start event
				_dispatchEvent(this, rootEl, 'start', dragEl, rootEl, oldIndex);
			}
		},

		_emulateDragOver: function () {
			if (touchEvt) {
				if (this._lastX === touchEvt.clientX && this._lastY === touchEvt.clientY) {
					return;
				}

				this._lastX = touchEvt.clientX;
				this._lastY = touchEvt.clientY;

				if (!supportCssPointerEvents) {
					_css(ghostEl, 'display', 'none');
				}

				var target = document.elementFromPoint(touchEvt.clientX, touchEvt.clientY),
					parent = target,
					groupName = ' ' + this.options.group.name + '',
					i = touchDragOverListeners.length;

				if (parent) {
					do {
						if (parent[expando] && parent[expando].options.groups.indexOf(groupName) > -1) {
							while (i--) {
								touchDragOverListeners[i]({
									clientX: touchEvt.clientX,
									clientY: touchEvt.clientY,
									target: target,
									rootEl: parent
								});
							}

							break;
						}

						target = parent; // store last element
					}
					/* jshint boss:true */
					while (parent = parent.parentNode);
				}

				if (!supportCssPointerEvents) {
					_css(ghostEl, 'display', '');
				}
			}
		},


		_onTouchMove: function (/**TouchEvent*/evt) {
			if (tapEvt) {
				// only set the status to dragging, when we are actually dragging
				if (!Sortable.active) {
					this._dragStarted();
				}

				// as well as creating the ghost element on the document body
				this._appendGhost();

				var touch = evt.touches ? evt.touches[0] : evt,
					dx = touch.clientX - tapEvt.clientX,
					dy = touch.clientY - tapEvt.clientY,
					translate3d = evt.touches ? 'translate3d(' + dx + 'px,' + dy + 'px,0)' : 'translate(' + dx + 'px,' + dy + 'px)';

				moved = true;
				touchEvt = touch;

				_css(ghostEl, 'webkitTransform', translate3d);
				_css(ghostEl, 'mozTransform', translate3d);
				_css(ghostEl, 'msTransform', translate3d);
				_css(ghostEl, 'transform', translate3d);

				evt.preventDefault();
			}
		},

		_appendGhost: function () {
			if (!ghostEl) {
				var rect = dragEl.getBoundingClientRect(),
					css = _css(dragEl),
					options = this.options,
					ghostRect;

				ghostEl = dragEl.cloneNode(true);

				_toggleClass(ghostEl, options.ghostClass, false);
				_toggleClass(ghostEl, options.fallbackClass, true);

				_css(ghostEl, 'top', rect.top - parseInt(css.marginTop, 10));
				_css(ghostEl, 'left', rect.left - parseInt(css.marginLeft, 10));
				_css(ghostEl, 'width', rect.width);
				_css(ghostEl, 'height', rect.height);
				_css(ghostEl, 'opacity', '0.8');
				_css(ghostEl, 'position', 'fixed');
				_css(ghostEl, 'zIndex', '100000');
				_css(ghostEl, 'pointerEvents', 'none');

				options.fallbackOnBody && document.body.appendChild(ghostEl) || rootEl.appendChild(ghostEl);

				// Fixing dimensions.
				ghostRect = ghostEl.getBoundingClientRect();
				_css(ghostEl, 'width', rect.width * 2 - ghostRect.width);
				_css(ghostEl, 'height', rect.height * 2 - ghostRect.height);
			}
		},

		_onDragStart: function (/**Event*/evt, /**boolean*/useFallback) {
			var dataTransfer = evt.dataTransfer,
				options = this.options;

			this._offUpEvents();

			if (activeGroup.pull == 'clone') {
				cloneEl = dragEl.cloneNode(true);
				_css(cloneEl, 'display', 'none');
				rootEl.insertBefore(cloneEl, dragEl);
			}

			if (useFallback) {

				if (useFallback === 'touch') {
					// Bind touch events
					_on(document, 'touchmove', this._onTouchMove);
					_on(document, 'touchend', this._onDrop);
					_on(document, 'touchcancel', this._onDrop);
				} else {
					// Old brwoser
					_on(document, 'mousemove', this._onTouchMove);
					_on(document, 'mouseup', this._onDrop);
				}

				this._loopId = setInterval(this._emulateDragOver, 50);
			}
			else {
				if (dataTransfer) {
					dataTransfer.effectAllowed = 'move';
					options.setData && options.setData.call(this, dataTransfer, dragEl);
				}

				_on(document, 'drop', this);
				setTimeout(this._dragStarted, 0);
			}
		},

		_onDragOver: function (/**Event*/evt) {
			var el = this.el,
				target,
				dragRect,
				revert,
				options = this.options,
				group = options.group,
				groupPut = group.put,
				isOwner = (activeGroup === group),
				canSort = options.sort;

			if (evt.preventDefault !== void 0) {
				evt.preventDefault();
				!options.dragoverBubble && evt.stopPropagation();
			}

			moved = true;

			if (activeGroup && !options.disabled &&
				(isOwner
					? canSort || (revert = !rootEl.contains(dragEl)) // Reverting item into the original list
					: activeGroup.pull && groupPut && (
						(activeGroup.name === group.name) || // by Name
						(groupPut.indexOf && ~groupPut.indexOf(activeGroup.name)) // by Array
					)
				) &&
				(evt.rootEl === void 0 || evt.rootEl === this.el) // touch fallback
			) {
				// Smart auto-scrolling
				_autoScroll(evt, options, this.el);

				if (_silent) {
					return;
				}

				target = _closest(evt.target, options.draggable, el);
				dragRect = dragEl.getBoundingClientRect();

				if (revert) {
					_cloneHide(true);

					if (cloneEl || nextEl) {
						rootEl.insertBefore(dragEl, cloneEl || nextEl);
					}
					else if (!canSort) {
						rootEl.appendChild(dragEl);
					}

					return;
				}


				if ((el.children.length === 0) || (el.children[0] === ghostEl) ||
					(el === evt.target) && (target = _ghostIsLast(el, evt))
				) {

					if (target) {
						if (target.animated) {
							return;
						}

						targetRect = target.getBoundingClientRect();
					}

					_cloneHide(isOwner);

					if (_onMove(rootEl, el, dragEl, dragRect, target, targetRect) !== false) {
						if (!dragEl.contains(el)) {
							el.appendChild(dragEl);
							parentEl = el; // actualization
						}

						this._animate(dragRect, dragEl);
						target && this._animate(targetRect, target);
					}
				}
				else if (target && !target.animated && target !== dragEl && (target.parentNode[expando] !== void 0)) {
					if (lastEl !== target) {
						lastEl = target;
						lastCSS = _css(target);
						lastParentCSS = _css(target.parentNode);
					}


					var targetRect = target.getBoundingClientRect(),
						width = targetRect.right - targetRect.left,
						height = targetRect.bottom - targetRect.top,
						floating = /left|right|inline/.test(lastCSS.cssFloat + lastCSS.display)
							|| (lastParentCSS.display == 'flex' && lastParentCSS['flex-direction'].indexOf('row') === 0),
						isWide = (target.offsetWidth > dragEl.offsetWidth),
						isLong = (target.offsetHeight > dragEl.offsetHeight),
						halfway = (floating ? (evt.clientX - targetRect.left) / width : (evt.clientY - targetRect.top) / height) > 0.5,
						nextSibling = target.nextElementSibling,
						moveVector = _onMove(rootEl, el, dragEl, dragRect, target, targetRect),
						after
					;

					if (moveVector !== false) {
						_silent = true;
						setTimeout(_unsilent, 30);

						_cloneHide(isOwner);

						if (moveVector === 1 || moveVector === -1) {
							after = (moveVector === 1);
						}
						else if (floating) {
							var elTop = dragEl.offsetTop,
								tgTop = target.offsetTop;

							if (elTop === tgTop) {
								after = (target.previousElementSibling === dragEl) && !isWide || halfway && isWide;
							} else {
								after = tgTop > elTop;
							}
						} else {
							after = (nextSibling !== dragEl) && !isLong || halfway && isLong;
						}

						if (!dragEl.contains(el)) {
							if (after && !nextSibling) {
								el.appendChild(dragEl);
							} else {
								target.parentNode.insertBefore(dragEl, after ? nextSibling : target);
							}
						}

						parentEl = dragEl.parentNode; // actualization

						this._animate(dragRect, dragEl);
						this._animate(targetRect, target);
					}
				}
			}
		},

		_animate: function (prevRect, target) {
			var ms = this.options.animation;

			if (ms) {
				var currentRect = target.getBoundingClientRect();

				_css(target, 'transition', 'none');
				_css(target, 'transform', 'translate3d('
					+ (prevRect.left - currentRect.left) + 'px,'
					+ (prevRect.top - currentRect.top) + 'px,0)'
				);

				target.offsetWidth; // repaint

				_css(target, 'transition', 'all ' + ms + 'ms');
				_css(target, 'transform', 'translate3d(0,0,0)');

				clearTimeout(target.animated);
				target.animated = setTimeout(function () {
					_css(target, 'transition', '');
					_css(target, 'transform', '');
					target.animated = false;
				}, ms);
			}
		},

		_offUpEvents: function () {
			var ownerDocument = this.el.ownerDocument;

			_off(document, 'touchmove', this._onTouchMove);
			_off(ownerDocument, 'mouseup', this._onDrop);
			_off(ownerDocument, 'touchend', this._onDrop);
			_off(ownerDocument, 'touchcancel', this._onDrop);
		},

		_onDrop: function (/**Event*/evt) {
			var el = this.el,
				options = this.options;

			clearInterval(this._loopId);
			clearInterval(autoScroll.pid);
			clearTimeout(this._dragStartTimer);

			// Unbind events
			_off(document, 'mousemove', this._onTouchMove);

			if (this.nativeDraggable) {
				_off(document, 'drop', this);
				_off(el, 'dragstart', this._onDragStart);
			}

			this._offUpEvents();

			if (evt) {
				if (moved) {
					evt.preventDefault();
					!options.dropBubble && evt.stopPropagation();
				}

				ghostEl && ghostEl.parentNode.removeChild(ghostEl);

				if (dragEl) {
					if (this.nativeDraggable) {
						_off(dragEl, 'dragend', this);
					}

					_disableDraggable(dragEl);

					// Remove class's
					_toggleClass(dragEl, this.options.ghostClass, false);
					_toggleClass(dragEl, this.options.chosenClass, false);

					if (rootEl !== parentEl) {
						newIndex = _index(dragEl, options.draggable);

						if (newIndex >= 0) {
							// drag from one list and drop into another
							_dispatchEvent(null, parentEl, 'sort', dragEl, rootEl, oldIndex, newIndex);
							_dispatchEvent(this, rootEl, 'sort', dragEl, rootEl, oldIndex, newIndex);

							// Add event
							_dispatchEvent(null, parentEl, 'add', dragEl, rootEl, oldIndex, newIndex);

							// Remove event
							_dispatchEvent(this, rootEl, 'remove', dragEl, rootEl, oldIndex, newIndex);
						}
					}
					else {
						// Remove clone
						cloneEl && cloneEl.parentNode.removeChild(cloneEl);

						if (dragEl.nextSibling !== nextEl) {
							// Get the index of the dragged element within its parent
							newIndex = _index(dragEl, options.draggable);

							if (newIndex >= 0) {
								// drag & drop within the same list
								_dispatchEvent(this, rootEl, 'update', dragEl, rootEl, oldIndex, newIndex);
								_dispatchEvent(this, rootEl, 'sort', dragEl, rootEl, oldIndex, newIndex);
							}
						}
					}

					if (Sortable.active) {
						if (newIndex === null || newIndex === -1) {
							newIndex = oldIndex;
						}

						_dispatchEvent(this, rootEl, 'end', dragEl, rootEl, oldIndex, newIndex);

						// Save sorting
						this.save();
					}
				}

			}
			this._nulling();
		},

		_nulling: function() {
			// Nulling
			rootEl =
			dragEl =
			parentEl =
			ghostEl =
			nextEl =
			cloneEl =

			scrollEl =
			scrollParentEl =

			tapEvt =
			touchEvt =

			moved =
			newIndex =

			lastEl =
			lastCSS =

			activeGroup =
			Sortable.active = null;
		},

		handleEvent: function (/**Event*/evt) {
			var type = evt.type;

			if (type === 'dragover' || type === 'dragenter') {
				if (dragEl) {
					this._onDragOver(evt);
					_globalDragOver(evt);
				}
			}
			else if (type === 'drop' || type === 'dragend') {
				this._onDrop(evt);
			}
		},


		/**
		 * Serializes the item into an array of string.
		 * @returns {String[]}
		 */
		toArray: function () {
			var order = [],
				el,
				children = this.el.children,
				i = 0,
				n = children.length,
				options = this.options;

			for (; i < n; i++) {
				el = children[i];
				if (_closest(el, options.draggable, this.el)) {
					order.push(el.getAttribute(options.dataIdAttr) || _generateId(el));
				}
			}

			return order;
		},


		/**
		 * Sorts the elements according to the array.
		 * @param  {String[]}  order  order of the items
		 */
		sort: function (order) {
			var items = {}, rootEl = this.el;

			this.toArray().forEach(function (id, i) {
				var el = rootEl.children[i];

				if (_closest(el, this.options.draggable, rootEl)) {
					items[id] = el;
				}
			}, this);

			order.forEach(function (id) {
				if (items[id]) {
					rootEl.removeChild(items[id]);
					rootEl.appendChild(items[id]);
				}
			});
		},


		/**
		 * Save the current sorting
		 */
		save: function () {
			var store = this.options.store;
			store && store.set(this);
		},


		/**
		 * For each element in the set, get the first element that matches the selector by testing the element itself and traversing up through its ancestors in the DOM tree.
		 * @param   {HTMLElement}  el
		 * @param   {String}       [selector]  default: `options.draggable`
		 * @returns {HTMLElement|null}
		 */
		closest: function (el, selector) {
			return _closest(el, selector || this.options.draggable, this.el);
		},


		/**
		 * Set/get option
		 * @param   {string} name
		 * @param   {*}      [value]
		 * @returns {*}
		 */
		option: function (name, value) {
			var options = this.options;

			if (value === void 0) {
				return options[name];
			} else {
				options[name] = value;

				if (name === 'group') {
					_prepareGroup(options);
				}
			}
		},


		/**
		 * Destroy
		 */
		destroy: function () {
			var el = this.el;

			el[expando] = null;

			_off(el, 'mousedown', this._onTapStart);
			_off(el, 'touchstart', this._onTapStart);

			if (this.nativeDraggable) {
				_off(el, 'dragover', this);
				_off(el, 'dragenter', this);
			}

			// Remove draggable attributes
			Array.prototype.forEach.call(el.querySelectorAll('[draggable]'), function (el) {
				el.removeAttribute('draggable');
			});

			touchDragOverListeners.splice(touchDragOverListeners.indexOf(this._onDragOver), 1);

			this._onDrop();

			this.el = el = null;
		}
	};


	function _cloneHide(state) {
		if (cloneEl && (cloneEl.state !== state)) {
			_css(cloneEl, 'display', state ? 'none' : '');
			!state && cloneEl.state && rootEl.insertBefore(cloneEl, dragEl);
			cloneEl.state = state;
		}
	}


	function _closest(/**HTMLElement*/el, /**String*/selector, /**HTMLElement*/ctx) {
		if (el) {
			ctx = ctx || document;

			do {
				if (
					(selector === '>*' && el.parentNode === ctx)
					|| _matches(el, selector)
				) {
					return el;
				}
			}
			while (el !== ctx && (el = el.parentNode));
		}

		return null;
	}


	function _globalDragOver(/**Event*/evt) {
		if (evt.dataTransfer) {
			evt.dataTransfer.dropEffect = 'move';
		}
		evt.preventDefault();
	}


	function _on(el, event, fn) {
		el.addEventListener(event, fn, false);
	}


	function _off(el, event, fn) {
		el.removeEventListener(event, fn, false);
	}


	function _toggleClass(el, name, state) {
		if (el) {
			if (el.classList) {
				el.classList[state ? 'add' : 'remove'](name);
			}
			else {
				var className = (' ' + el.className + ' ').replace(RSPACE, ' ').replace(' ' + name + ' ', ' ');
				el.className = (className + (state ? ' ' + name : '')).replace(RSPACE, ' ');
			}
		}
	}


	function _css(el, prop, val) {
		var style = el && el.style;

		if (style) {
			if (val === void 0) {
				if (document.defaultView && document.defaultView.getComputedStyle) {
					val = document.defaultView.getComputedStyle(el, '');
				}
				else if (el.currentStyle) {
					val = el.currentStyle;
				}

				return prop === void 0 ? val : val[prop];
			}
			else {
				if (!(prop in style)) {
					prop = '-webkit-' + prop;
				}

				style[prop] = val + (typeof val === 'string' ? '' : 'px');
			}
		}
	}


	function _find(ctx, tagName, iterator) {
		if (ctx) {
			var list = ctx.getElementsByTagName(tagName), i = 0, n = list.length;

			if (iterator) {
				for (; i < n; i++) {
					iterator(list[i], i);
				}
			}

			return list;
		}

		return [];
	}



	function _dispatchEvent(sortable, rootEl, name, targetEl, fromEl, startIndex, newIndex) {
		var evt = document.createEvent('Event'),
			options = (sortable || rootEl[expando]).options,
			onName = 'on' + name.charAt(0).toUpperCase() + name.substr(1);

		evt.initEvent(name, true, true);

		evt.to = rootEl;
		evt.from = fromEl || rootEl;
		evt.item = targetEl || rootEl;
		evt.clone = cloneEl;

		evt.oldIndex = startIndex;
		evt.newIndex = newIndex;

		rootEl.dispatchEvent(evt);

		if (options[onName]) {
			options[onName].call(sortable, evt);
		}
	}


	function _onMove(fromEl, toEl, dragEl, dragRect, targetEl, targetRect) {
		var evt,
			sortable = fromEl[expando],
			onMoveFn = sortable.options.onMove,
			retVal;

		evt = document.createEvent('Event');
		evt.initEvent('move', true, true);

		evt.to = toEl;
		evt.from = fromEl;
		evt.dragged = dragEl;
		evt.draggedRect = dragRect;
		evt.related = targetEl || toEl;
		evt.relatedRect = targetRect || toEl.getBoundingClientRect();

		fromEl.dispatchEvent(evt);

		if (onMoveFn) {
			retVal = onMoveFn.call(sortable, evt);
		}

		return retVal;
	}


	function _disableDraggable(el) {
		el.draggable = false;
	}


	function _unsilent() {
		_silent = false;
	}


	/** @returns {HTMLElement|false} */
	function _ghostIsLast(el, evt) {
		var lastEl = el.lastElementChild,
				rect = lastEl.getBoundingClientRect();

		return ((evt.clientY - (rect.top + rect.height) > 5) || (evt.clientX - (rect.right + rect.width) > 5)) && lastEl; // min delta
	}


	/**
	 * Generate id
	 * @param   {HTMLElement} el
	 * @returns {String}
	 * @private
	 */
	function _generateId(el) {
		var str = el.tagName + el.className + el.src + el.href + el.textContent,
			i = str.length,
			sum = 0;

		while (i--) {
			sum += str.charCodeAt(i);
		}

		return sum.toString(36);
	}

	/**
	 * Returns the index of an element within its parent for a selected set of
	 * elements
	 * @param  {HTMLElement} el
	 * @param  {selector} selector
	 * @return {number}
	 */
	function _index(el, selector) {
		var index = 0;

		if (!el || !el.parentNode) {
			return -1;
		}

		while (el && (el = el.previousElementSibling)) {
			if (el.nodeName.toUpperCase() !== 'TEMPLATE'
					&& _matches(el, selector)) {
				index++;
			}
		}

		return index;
	}

	function _matches(/**HTMLElement*/el, /**String*/selector) {
		if (el) {
			selector = selector.split('.');

			var tag = selector.shift().toUpperCase(),
				re = new RegExp('\\s(' + selector.join('|') + ')(?=\\s)', 'g');

			return (
				(tag === '' || el.nodeName.toUpperCase() == tag) &&
				(!selector.length || ((' ' + el.className + ' ').match(re) || []).length == selector.length)
			);
		}

		return false;
	}

	function _throttle(callback, ms) {
		var args, _this;

		return function () {
			if (args === void 0) {
				args = arguments;
				_this = this;

				setTimeout(function () {
					if (args.length === 1) {
						callback.call(_this, args[0]);
					} else {
						callback.apply(_this, args);
					}

					args = void 0;
				}, ms);
			}
		};
	}

	function _extend(dst, src) {
		if (dst && src) {
			for (var key in src) {
				if (src.hasOwnProperty(key)) {
					dst[key] = src[key];
				}
			}
		}

		return dst;
	}


	// Export utils
	Sortable.utils = {
		on: _on,
		off: _off,
		css: _css,
		find: _find,
		is: function (el, selector) {
			return !!_closest(el, selector, el);
		},
		extend: _extend,
		throttle: _throttle,
		closest: _closest,
		toggleClass: _toggleClass,
		index: _index
	};


	/**
	 * Create sortable instance
	 * @param {HTMLElement}  el
	 * @param {Object}      [options]
	 */
	Sortable.create = function (el, options) {
		return new Sortable(el, options);
	};


	// Export
	Sortable.version = '1.4.2';
	return Sortable;
});

},{}]},{},[32])


//# sourceMappingURL=bundle.js.map
