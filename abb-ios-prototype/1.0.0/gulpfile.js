/*!
 * This file is part of abb-ui-prototype.
 *
 * abb-ui-prototype is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * abb-ui-prototype is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with abb-ui-prototype.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable no-console */

var gulp = require('gulp');
var browserify = require('browserify');
var stringify = require('stringify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var scss = require('gulp-sass');
var rename = require('gulp-rename');
var sourcemap = require('gulp-sourcemaps');
var connect = require('gulp-connect');

var jsMapsConfig = {
  includeContent: false,
  sourceRoot: '/js'
};

var scssMapsConfig = {
  includeContent: false,
  sourceRoot: '/scss'
};

gulp.task('js', function () {
  browserify({
    debug: true,
    basedir: 'js'
  }).transform(stringify, {
    appliesTo: {
      includeExtensions: ['.html']
    }
  })
    .add('main.js')
    .bundle()
    .on('error', function (err) {
      console.log(err.toString());
      this.emit('end');
    })
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemap.init({loadMaps: true, debug: true}))
    .pipe(sourcemap.write('.', jsMapsConfig))
    .pipe(gulp.dest('.'));
});

gulp.task('scss', function () {
  return gulp.src('scss/main.scss')
    .pipe(sourcemap.init())
    .pipe(scss().on('error', scss.logError))
    .pipe(rename('bundle.css'))
    .pipe(sourcemap.write('.', scssMapsConfig))
    .pipe(gulp.dest('.'));
});

gulp.task('watch', ['scss', 'js'], function () {
  gulp.watch(['scss/**/*.scss'], ['scss']);
  gulp.watch(['js/**/*.js', 'html/**/*.html'], ['js']);
});

gulp.task('connect', ['watch'], function () {
  return connect.server({
    root: '.'
  });
});

gulp.task('default', ['connect']);
