
# abb-ui-prototype

Adblock Browser user interface prototype

## Usage

### Dependencies

- [Node.js](https://nodejs.org)
- [Gulp](http://gulpjs.com/) `npm i -g gulp`

### Development

- Install node modules `npm i`
- Run default gulp task `gulp`

## Contributors

### Maintainers

- Julian Doucette <julian@adblockplus.org>
- Aaron Thornburgh <aaron@adblockplus.org>

### Developers

- Julian Doucette <julian@adblockplus.org>

### UX Designers

- Aaron Thornburgh <aaron@adblockplus.org>

### Graphics Designers

- Christiane Stelberg <christiane@adblockplus.org>

## Copyright

- [Eyeo GmbH](https://eyeo.com/)

## Licence

[GNU Affero General Public License Version 3 or later](http://www.gnu.org/licenses/agpl-3.0.en.html)
