
var Backbone = require("backbone");

var PageModel = require("../models/page");

var pages = require("./pages");

var FavoritesModel = Backbone.Collection.extend({
  model: PageModel
});

module.exports = new FavoritesModel([
  pages.get(1),
  pages.get(2)
]);
