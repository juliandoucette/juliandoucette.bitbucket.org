
var Backbone = require("backbone");

var PageModel = require("../models/page");

var WhitelistCollection = Backbone.Collection.extend({
  model: PageModel
});

module.exports = new WhitelistCollection();
