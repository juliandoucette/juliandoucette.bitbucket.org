
var Backbone = require("backbone");

var PageModel = require("../models/page");

var pages = require("./pages");
var state = require("../models/state");

var HistoryCollection = Backbone.Collection.extend({

  model: PageModel,

  initialize: function()
  {
    this.listenTo(state, "change:page", function(_, cid)
    {
      if (!this.get(cid))
      {
        var page = pages.get(cid);

        if (!page.get("isInternal"))
        {
          this.add(pages.get(cid));
        }
      }
    });
  }

});

module.exports = new HistoryCollection([
  pages.get(1),
  pages.get(2),
  pages.createItem(),
  pages.createItem(),
  pages.createItem()
]);
