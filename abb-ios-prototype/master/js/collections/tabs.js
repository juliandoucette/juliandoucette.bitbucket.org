
var Backbone = require("backbone");

var TabModel = require("../models/tab");

var TabsCollection = Backbone.Collection.extend({

  initialize: function()
  {
    // custom lengths (rather than separating collections)
    this.normalLength = 1;
    this.ghostLength = 1;

    // increment length on add
    this.on("add", function(tab)
    {
      if (tab.get("mode") === "ghostmode")
      {
        this.ghostLength++;
      }
      else
      {
        this.normalLength++;
      }
    }, this);

    // decrement length on remove
    this.on("remove", function(tab)
    {
      if (tab.get("mode") === "ghostmode")
      {
        this.ghostLength--;
      }
      else
      {
        this.normalLength--;
      }
    }, this);
  },

  model: TabModel

});

module.exports = new TabsCollection([

  // default to 1 New normal && 1 new ghost

  {
    mode: "normalmode",
    page: "normalmode"
  },
  {
    mode: "ghostmode",
    page: "ghostmode"
  }

]);
