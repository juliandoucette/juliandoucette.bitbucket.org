
var Backbone = require("backbone");

var PageModel = require("../models/page");

// length starts at 2 because ids 1 && 2 are defined below
var length = 2;

var PageCollection = Backbone.Collection.extend({

  model: PageModel,

  // used to create pages based on auto incrementing id
  createItem: function()
  {
    length++;
    return this.add({id: length});
  }

});

var collection = new PageCollection([

  // default (New tab) normal / ghost mode pages

  {
    id: "normalmode",
    title: "New tab",
    location: "",
    isInternal: true,
    src: "pages/normalmode/index.html"
  },
  {
    id: "ghostmode",
    title: "New tab",
    location: "",
    isInternal: true,
    src: "pages/ghostmode/index.html"
  },

  // pages linked on normalmode dashboard (New tab)

  {
    id: 1
  },
  {
    id: 2
  }

]);

module.exports = collection;
