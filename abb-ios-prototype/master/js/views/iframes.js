
var $ = require("jquery");
var _ = require("underscore");
var Backbone = require("backbone");

var state = require("../models/state");
var tabs = require("../collections/tabs");
var pages = require("../collections/pages");
var whitelist = require("../collections/whitelist");

var IframesView = Backbone.View.extend({

  initialize: function()
  {
    // switch tab when tab changes
    this.listenTo(state, "change:normalmode change:ghostmode", function(_, tabId)
    {
      this.switchItem(tabId);
    });

    // switch tab when mode changes
    this.listenTo(state, "change:mode", function(_, mode)
    {
      this.switchItem(state.get(mode));
    });

    // update tab src when tab page changes
    this.listenTo(tabs, "change:page", function(tab, pageId)
    {
      this.updateItem(
        tab,
        pages.get(pageId)
      );
    });

    // update tab srcs when whitelist changes
    this.listenTo(whitelist, "add remove", function(page)
    {
      _.each(tabs.where({page: page.id}), function(tab)
      {
        this.updateItem(
          tab,
          page
        );
      }.bind(this));
    });

    // create iframe for tab
    this.listenTo(tabs, "add", function(tab)
    {
      this.createItem(
        tab,
        pages.get(tab.get("page"))
      );
    });

    // remove iframe for tab
    this.listenTo(tabs, "remove", function(tab)
    {
      this.removeItem(tab);
    });

    // create iframes for default tabs
    tabs.each(function(tab)
    {
      this.createItem(
        tab,
        pages.get(tab.get("page"))
      );
    }, this);

    // switch to default tab
    this.switchItem(tabs.findWhere({mode: state.MODE.NORMAL}).cid);
  },

  el: ".iframes-view",

  getSrc: function(page)
  {
    return page.get("src") +
      "?id=" + page.id +
      "&ads=" + (whitelist.get(page.id) ? 1 : 0);
  },

  createItem: function(tab, page)
  {
    var iframe = document.createElement("iframe");
    iframe.className = "iframe " + tab.cid;
    iframe.src = this.getSrc(page);

    iframe.addEventListener("load", function()
    {
      $(iframe.contentDocument).on("click", "a", function(event)
      {
        var href = event.currentTarget.getAttribute("href");

        if (href)
        {
          event.preventDefault();
          state.gotoLocation(href);
        }
      }.bind(this));
    }.bind(this));


    this.$el.append(iframe);
  },

  removeItem: function(tab)
  {
    this.$("." + tab.cid).remove();
  },

  switchItem: function(id)
  {
    var $prev = this.$(".active");
    var $next = this.$("." + id);

    if ($prev)
    {
      $prev.removeClass("active");
    }

    $next.addClass("active");
  },

  updateItem: function(tab, page)
  {
    var iframe = this.$("." + tab.cid).get(0);
    iframe.src = this.getSrc(page);
  }

});

module.exports = new IframesView();
