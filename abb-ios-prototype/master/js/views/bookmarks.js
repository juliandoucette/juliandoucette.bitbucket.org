
var _ = require("underscore");

var SlipView = require("./slip");

var state = require("../models/state");
var favorites = require("../collections/favorites");

var BookmarksView = SlipView.extend({

  initialize: function()
  {
    SlipView.prototype.initialize.apply(this, arguments);

    this.listenTo(favorites, "add", function(page)
    {
      this.createItem(page.attributes);
    });

    this.listenTo(favorites, "remove", function(page)
    {
      this.removeItem(page.id);
    });

    this.listenTo(state, "change:view", function(_, view)
    {
      if (view === state.VIEW.BOOKMARKS)
      {
        this.open();
      }
      else if (state.previous("view") === state.VIEW.BOOKMARKS)
      {
        this.close();
      }
    });

    favorites.each(function(favorite)
    {
      this.createItem(favorite.attributes);
    }.bind(this));
  },

  events: _.extend({
    // on swipe away
    "slip:afterswipe .slip-item": function(event)
    {
      favorites.remove(event.currentTarget.getAttribute("data-id"));
    },

    // on select
    "slip:tap .slip-item": function(event)
    {
      // set tab page
      var page = favorites.get(event.currentTarget.getAttribute("data-id"));
      state.getTab().set({page: page.id});

      // close selection modal
      state.set({view: state.VIEW.BROWSER});
    }
  }, SlipView.prototype.events),

  el: ".bookmarks-view",

  open: function()
  {
    this.$el.removeClass("slideOutRight");
    this.$el.addClass("slideInRight animated");
  },

  close: function()
  {
    this.$el.removeClass("slideInRight");
    this.$el.addClass("slideOutRight animated");
  }

});

module.exports = new BookmarksView();
