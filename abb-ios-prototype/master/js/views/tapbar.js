
var Backbone = require("backbone");

var state = require("../models/state");
var tabs = require("../collections/tabs");

var TapbarView = Backbone.View.extend({

  initialize: function()
  {
    // select tapbar item on view change
    this.listenTo(state, "change:view", function(_, view)
    {
      if (view === state.VIEW.BOOKMARKS)
      {
        this.selectFavorites();
      }
      else if (state.get("mode") === state.MODE.NORMAL)
      {
        this.selectNormal();
      }
      else if (state.get("mode") === state.MODE.GHOST)
      {
        this.selectGhost();
      }
    });

    // select tapbar item on mode change
    this.listenTo(state, "change:mode", function(_, mode)
    {
      if (mode === state.MODE.GHOST)
      {
        this.selectGhost();
      }
      else
      {
        this.selectNormal();
      }
    });

    // update tabs count
    this.listenTo(tabs, "add remove", function(tab)
    {
      if (tab.get("mode") === state.MODE.GHOST)
      {
        this.setGhostLength(tabs.ghostLength);
      }
      else
      {
        this.setNormalLength(tabs.normalLength);
      }
    });

    // toggle back when available
    this.listenTo(state, "change:canBack", function(_, canBack)
    {
      this.$(".tapbar-back").prop("disabled", !canBack);
    });

    // toggle forward when available
    this.listenTo(state, "change:canForward", function(_, canForward)
    {
      this.$(".tapbar-forward").prop("disabled", !canForward);
    });
  },

  el: ".tapbar-view",

  events: {

    "click .tapbar-back": function()
    {
      state.set({view: state.VIEW.BROWSER});
      state.getTab().goBack();
    },

    "click .tapbar-forward": function()
    {
      state.set({view: state.VIEW.BROWSER});
      state.getTab().goForward();
    },

    "click .tapbar-normal": function()
    {
      var fromMode = state.get("mode");

      var fromView = state.get("view");

      if (
        fromMode !== state.MODE.NORMAL ||
        fromView === state.VIEW.NORMAL ||
        fromView === state.VIEW.BOOKMARKS
      )
      {
        state.set({view: state.VIEW.BROWSER});
      }
      else
      {
        state.set({view: state.VIEW.NORMAL});
      }

      if (fromMode !== state.MODE.NORMAL)
      {
        state.set("mode", state.MODE.NORMAL);
      }
    },

    "click .tapbar-ghost": function()
    {
      var fromMode = state.get("mode");

      var fromView = state.get("view");

      if (
        fromMode !== state.MODE.GHOST ||
        fromView === state.VIEW.GHOST ||
        fromView === state.VIEW.BOOKMARKS
      )
      {
        state.set({view: state.VIEW.BROWSER});
      }
      else
      {
        state.set({view: state.VIEW.GHOST});
      }

      if (fromMode !== state.MODE.GHOST)
      {
        state.set({mode: state.MODE.GHOST});
      }
    },

    "click .tapbar-favorites": function()
    {
      if (state.get("view") === state.VIEW.BOOKMARKS)
      {
        state.set({view: state.VIEW.BROWSER});
      }
      else
      {
        state.set({view: state.VIEW.BOOKMARKS});
      }
    }

  },

  selectNormal: function()
  {
    this.$(".tapbar-normal i").attr("class", "icon-tabs-filled");
    this.$(".tapbar-ghost i").attr("class", "icon-ghost-tabs");
    this.$(".tapbar-favorites i").first().attr("class", "icon-bookmark");
  },

  selectGhost: function()
  {
    this.$(".tapbar-normal i").attr("class", "icon-tabs");
    this.$(".tapbar-ghost i").attr("class", "icon-ghost-tabs-filled");
    this.$(".tapbar-favorites i").first().attr("class", "icon-bookmark");
  },

  selectFavorites: function()
  {
    this.$(".tapbar-normal i").attr("class", "icon-tabs");
    this.$(".tapbar-ghost i").attr("class", "icon-ghost-tabs");
    this.$(".tapbar-favorites i").first().attr("class", "icon-bookmark-filled");
  },

  setGhostLength: function(length)
  {
    this.$(".tapbar-ghost span").text(length);
  },

  setNormalLength: function(length)
  {
    this.$(".tapbar-normal span").text(length);
  }

});

module.exports = new TapbarView();
