
var Backbone = require("backbone");

var state = require("../models/state");

var AddressbarView = Backbone.View.extend({

  initialize: function()
  {
    // enable / disable when toggeling tabs views
    this.listenTo(state, "change:view", function(_, view)
    {
      if (view === state.VIEW.NORMAL || view === state.VIEW.GHOST)
      {
        this.disable();
      }
      else
      {
        this.enable(state.get("isInternal"));
      }
    });

    // update location when page changes
    this.listenTo(state, "change:location", function(_, location)
    {
      this.setLocation(location);
    });

    // enable / disable favorite based on page.isInternal
    this.listenTo(state, "change:isInternal", function(_, isInternal)
    {
      if (isInternal)
      {
        this.disableFavorite();
      }
      else
      {
        this.enableFavorite();
      }
    });

    // update favorite icon when favorite is toggled
    this.listenTo(state, "change:isFavorited", function(_, isFavorited)
    {
      if (isFavorited)
      {
        this.favorite();
      }
      else
      {
        this.unfavorite();
      }
    });
  },

  el: ".addressbar-view",

  events: {

    "submit": function(event)
    {
      event.preventDefault();
      state.gotoLocation(this.getLocation());
      this.blur();
    },

    "blur .addressbar-location": function()
    {
      this.setLocation(state.get("location"));
      this.deselectLocation();
    },

    "focus .addressbar-location": function()
    {
      this.selectLocation();
    },

    "click .addressbar-cancel": function()
    {
      this.setLocation(state.get("location"));
      this.blur();
    },

    "click .addressbar-menu": function(event)
    {
      event.stopPropagation();
      state.toggleMenu();
    },

    "click .addressbar-favorite": function()
    {
      state.toggleFavorite();
    }
  },

  getLocation: function()
  {
    return this.$(".addressbar-location").val();
  },

  setLocation: function(value)
  {
    this.$(".addressbar-location").val(value);
  },

  blur: function()
  {
    this.$(".addressbar-location").get(0).blur();
  },

  selectLocation: function()
  {
    this.$el.addClass("selected");
    this.$(".addressbar-location").get(0).select();
  },

  deselectLocation: function()
  {
    this.$el.removeClass("selected");
    window.getSelection().removeAllRanges();
  },

  enable: function(isInternal)
  {
    this.$el.removeClass("disabled");
    this.$(".addressbar-location").prop("disabled", false);

    // only enable favorite on external pages
    if (!isInternal)
    {
      this.$(".addressbar-favorite").prop("disabled", false);
    }
  },

  disable: function()
  {
    this.$el.addClass("disabled");
    this.$(".addressbar-favorite").prop("disabled", true);
    this.$(".addressbar-location").prop("disabled", true);
  },

  enableFavorite: function()
  {
    this.$(".addressbar-favorite").prop("disabled", false);
  },

  disableFavorite: function()
  {
    this.$(".addressbar-favorite").prop("disabled", true);
  },

  favorite: function()
  {
    this.$(".addressbar-favorite")
      .children()
      .first()
      .attr("class", "icon-favorite-filled");
  },

  unfavorite: function()
  {
    this.$(".addressbar-favorite")
      .children()
      .first()
      .attr("class", "icon-favorite");
  }

});

module.exports = new AddressbarView();
