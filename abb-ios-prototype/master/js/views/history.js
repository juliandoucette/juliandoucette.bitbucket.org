
var $ = require("jquery");
var _ = require("underscore");

var SlipView = require("./slip");

var state = require("../models/state");
var history = require("../collections/history");
var favorites = require("../collections/favorites");

var HistoryView = SlipView.extend({

  initialize: function()
  {
    SlipView.prototype.initialize.apply(this, arguments);

    this.template = _.template($(".history-item-template").html());

    this.listenTo(history, "add", function(page)
    {
      this.createItem(page.attributes);
    });

    this.listenTo(history, "remove", function(page)
    {
      this.removeItem(page.id);
    });

    this.listenTo(state, "change:view", function(_, view)
    {
      if (view === state.VIEW.HISTORY)
      {
        this.open();
      }
      else if (state.previous("view") === state.VIEW.HISTORY)
      {
        this.close();
      }
    });

    this.listenTo(favorites, "add", function(model)
    {
      this.setBookmark(model.id, true);
    });

    this.listenTo(favorites, "remove", function(model)
    {
      this.setBookmark(model.id, false);
    });

    history.each(function(item)
    {
      this.createItem(item.attributes);
    }.bind(this));

    favorites.each(function(model)
    {
      this.setBookmark(model.id, true);
    }.bind(this));
  },

  el: ".history-view",

  events: _.extend({

    "click .item-favorite": function(event)
    {
      var id = event.currentTarget.getAttribute("data-id");

      if (favorites.get(id))
      {
        favorites.remove(id);
      }
    },
    "click .history-back": function()
    {
      state.set({view: state.VIEW.BROWSER});
    },

    "click .history-delete": function()
    {
      history.remove(history.models);
    },

    // prevent date from sorting
    "slip:beforereorder .history-date": function(event)
    {
      event.preventDefault();
    },

    // on swipe away
    "slip:afterswipe .slip-item": function(event)
    {
      history.remove(event.currentTarget.getAttribute("data-id"));
    },

    // on select
    "slip:tap .slip-item": function(event)
    {
      // set tab page
      var page = history.get(event.currentTarget.getAttribute("data-id"));
      state.getTab().set({page: page.id});

      // close selection modal
      state.set({view: state.VIEW.BROWSER});
    }
  }, SlipView.prototype.events),

  open: function()
  {
    this.$el.removeClass("slideOutRight");
    this.$el.addClass("slideInRight animated");
  },

  close: function()
  {
    this.$el.removeClass("slideInRight");
    this.$el.addClass("slideOutRight animated");
  },

  setBookmark: function(pageId, isFavorited)
  {
    this.$("a[data-id='$id']".replace("$id", pageId))
      .toggleClass("active", isFavorited);
  }
});

module.exports = new HistoryView();
