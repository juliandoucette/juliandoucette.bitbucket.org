
var _ = require("underscore");
var $ = require("jquery");
var Backbone = require("backbone");
var slipjs = require("slipjs");

// slipjs exports to window
var Slip = window.Slip || slipjs;

var SlipView = Backbone.View.extend({

  initialize: function()
  {
    this._slip = new Slip(this.$(".slip-list").get(0), {
      keepSwipingPercent: 50,
      minimumSwipeVelocity: 0.5
    });

    this.template = _.template($(".slip-item-template").html());
  },

  events: {

    "slip:reorder .slip-item": function(event)
    {
      event.currentTarget.parentNode.insertBefore(
        event.currentTarget,
        event.detail.insertBefore
      );
    }
  },

  createItem: function(item)
  {
    var $node = $(document.createElement("li"));
    $node
      .attr("data-id", item.id)
      .addClass([item.id, "slip-item"].join(" "))
      .html(this.template(item))
      .appendTo(this.$(".slip-list"));
  },

  removeItem: function(itemId)
  {
    this.$(".slip-list ." + itemId).remove();
  },

  updateItem: function(item)
  {
    this.$("." + item.id).html(this.template(item));
  },

  destroyAll: function()
  {
    this.$(".slip-list").html("");
  }
});

module.exports = SlipView;
