
var Backbone = require("backbone");

var state = require("../models/state");

var ContainerView = Backbone.View.extend({

  initialize: function()
  {
    this.listenTo(state, "change:mode", function(_, mode)
    {
      if (typeof StatusBar !== "undefined")
      {
        if (mode === state.MODE.GHOST)
        {
          StatusBar.backgroundColorByName("black");
          StatusBar.styleBlackOpaque();
        }
        else
        {
          StatusBar.backgroundColorByName("white");
          StatusBar.styleDefault();
        }
      }
    });

    // update mode class
    this.listenTo(state, "change:mode", function(_, to)
    {
      this.setMode(to, state.previous("mode"));
    });

    // update view class
    this.listenTo(state, "change:view", function(_, to)
    {
      this.setView(to, state.previous("view"));
    });

    // update whitelisted class
    this.listenTo(state, "change:isWhitelisted", function(_, isWhitelisted)
    {
      this.setWhitelisted(isWhitelisted);
    });

    // update favorited class
    this.listenTo(state, "change:isFavorited", function(_, isFavorited)
    {
      this.setFavorited(isFavorited);
    });

    // update menu open class
    this.listenTo(state, "change:isMenuOpen", function(_, isOpen)
    {
      this.setMenuOpen(isOpen);
    });

    // update internal class
    this.listenTo(state, "change:isInternal", function(_, isInternal)
    {
      this.setInternal(isInternal);
    });
  },

  el: ".container-view",

  events: {
    "click": function()
    {
      state.set("isMenuOpen", false);
    },

    "slip:beforeswipe": function()
    {
      this.$el.addClass("swipeing");
    },

    "slip:cancelswipe": function()
    {
      this.$el.removeClass("swipeing");
    },

    "slip:afterswipe": function()
    {
      this.$el.removeClass("swipeing");
    }
  },

  setMode: function(to, from)
  {
    this.$el.removeClass(from);
    this.$el.addClass(to);
  },

  setView: function(to, from)
  {
    this.$el.removeClass(from);
    this.$el.addClass(to);
  },

  setInternal: function(isInternal)
  {
    this.$el.toggleClass("internal", isInternal);
  },

  setMenuOpen: function(isOpen)
  {
    this.$el.toggleClass("menuopen", isOpen);
  },

  setWhitelisted: function(isWhitelisted)
  {
    this.$el.toggleClass("whitelisted", isWhitelisted);
  },

  setFavorited: function(isFavorited)
  {
    this.$el.toggleClass("favorited", isFavorited);
  }

});

module.exports = new ContainerView();
