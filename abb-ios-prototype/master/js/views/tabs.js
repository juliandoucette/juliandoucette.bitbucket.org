
var _ = require("underscore");
var SlipView = require("./slip");

var state = require("../models/state");
var tabs = require("../collections/tabs");
var pages = require("../collections/pages");

var TabsView = SlipView.extend({

  initialize: function(options)
  {
    // initialize slip list
    SlipView.prototype.initialize.apply(this, arguments);

    // TabsView is used for normaltabs and ghosttabs by passing mode/view
    this.mode = options.mode;
    this.view = options.view;

    //create default tab in view
    var tab = tabs.findWhere({mode: this.mode});
    var page = pages.get(tab.get("page"));

    this.createItem({
      id: tab.cid,
      title: page.get("title"),
      location: page.get("location")
    });

    // toggle modal and ensure active tab
    this.listenTo(state, "change:view", function(_, view)
    {
      if (state.get("mode") === this.mode)
      {
        if (view === this.view)
        {
          this.open();
        }
        else if (state.previous("view") === this.view)
        {
          // ensure that there is an active tab to reveal

          var length = this.mode === state.MODE.GHOST ?
            tabs.ghostLength : tabs.normalLength;

          var cid;

          // if there are no more tabs
          if (length < 1)
          {
            // create one
            cid = tabs.add({mode: this.mode, page: this.mode}).cid;
          }
          // else if the active tab has been removed
          else if (!tabs.get(state.get(state.get("mode"))))
          {
            var history = this.mode === state.MODE.GHOST ?
              state.ghosthistory : state.normalhistory;

            // get the last active one
            cid = history[history.length - 1];
          }

          if (cid)
          {
            // switch to tab
            state.set(state.get("mode"), cid);
          }

          this.close();
        }
      }
    });

    // create tabs
    this.listenTo(tabs, "add", function(tab)
    {
      if (state.get("mode") === this.mode)
      {
        var page = pages.get(tab.get("page"));

        this.createItem({
          id: tab.cid,
          title: page.get("title"),
          location: page.get("location")
        });
      }
    });

    // remove tabs
    this.listenTo(tabs, "remove", function(tab)
    {
      if (state.get("mode") === this.mode)
      {
        this.removeItem(tab.cid);
      }
    });

    // update tabs
    this.listenTo(tabs, "change:page", function(tab)
    {
      if (state.get("mode") === this.mode)
      {
        var page = pages.get(tab.get("page"));

        this.updateItem({
          id: tab.cid,
          title: page.get("title"),
          location: page.get("location")
        });
      }
    });
  },

  events: _.extend({

    // on click create
    "click .tabs-create": function()
    {
      var tab = tabs.add({mode: this.mode, page: this.mode});

      // this.mode is the active tab for this mode in state

      // switch tab
      state.set(this.mode, tab.cid);
      // close selection modal
      state.set({view: state.VIEW.BROWSER});
    },

    // on swipe away
    "slip:afterswipe .slip-item": function(event)
    {
      //remove tab
      tabs.remove(event.currentTarget.getAttribute("data-id"));
    },

    // on select
    "slip:tap .slip-item": function(event)
    {
      // this.mode is the active tab for this mode in state

      //switch tab
      state.set(this.mode, event.currentTarget.getAttribute("data-id"));
      // close selection modal
      state.set({view: state.VIEW.BROWSER});
    }

  }, SlipView.prototype.events),

  open: function()
  {
    this.$el.removeClass("slideOutDown");
    this.$el.addClass("slideInUp animated");
  },

  close: function()
  {
    this.$el.removeClass("slideInUp");
    this.$el.addClass("slideOutDown animated");
  }

});

module.exports = {
  normal: new TabsView({
    el: ".normal-tabs-view",
    mode: state.MODE.NORMAL,
    view: state.VIEW.NORMAL
  }),
  ghost: new TabsView({
    el: ".ghost-tabs-view",
    mode: state.MODE.GHOST,
    view: state.VIEW.GHOST
  })
};
