
var Backbone = require("backbone");

var state = require("../models/state");

var MenuView = Backbone.View.extend({

  el: ".menu-view",

  initialize: function()
  {
    // toggle menu
    this.listenTo(state, "change:isMenuOpen", function(_, isOpen)
    {
      if (isOpen)
      {
        this.open();
      }
      else
      {
        this.close();
      }
    });

    // update enable/disabled options
    this.listenTo(state, "change:isInternal", function(_, isInternal)
    {
      this.update(isInternal);
    });

    // update favorite icon
    this.listenTo(state, "change:isFavorited", function(_, isFavorited)
    {
      if (isFavorited)
      {
        this.favorite();
      }
      else
      {
        this.unfavorite();
      }
    });
  },

  events: {

    "animationend": function()
    {
      this.$el.removeClass("animating");
    },

    "click .menu-whitelist": function()
    {
      state.toggleWhitelisted();
    },

    "click .menu-favorite": function()
    {
      state.toggleFavorite();
    },

    "click .menu-share": function()
    {
      alert("This feature is unavailable in this demo.");
    },

    "click .menu-history": function()
    {
      state.set({view: state.VIEW.HISTORY});
    },

    "click .menu-settings": function()
    {
      alert("This feature is unavailable in this demo.");
    }
  },

  open: function()
  {
    this.$el.removeClass("slideOutUp");
    this.$el.addClass("slideInDown animated animating");
  },

  close: function()
  {
    this.$el.removeClass("slideInDown");
    this.$el.addClass("slideOutUp animated animating");
  },

  favorite: function()
  {
    this.$(".menu-favorite i").attr("class", "icon-favorite-filled");
    this.$(".menu-favorite span").text("Page bookmarked");
  },

  unfavorite: function()
  {
    this.$(".menu-favorite i").attr("class", "icon-favorite");
    this.$(".menu-favorite span").text("Bookmark page");
  },

  update: function(isInternal)
  {
    this.$(".menu-refresh").prop("disabled", isInternal);
    this.$(".menu-whitelist").prop("disabled", isInternal);
    this.$(".menu-favorite").prop("disabled", isInternal);
    this.$(".menu-share").prop("disabled", isInternal);
  }

});

module.exports = new MenuView();
