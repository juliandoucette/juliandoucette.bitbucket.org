
var _ = require("underscore");
var Backbone = require("backbone");

var tabs = require("../collections/tabs");
var pages = require("../collections/pages");
var favorites = require("../collections/favorites");
var whitelist = require("../collections/whitelist");

var defaultNormalTab = tabs.findWhere({mode: "normalmode"});
var defaultGhostTab = tabs.findWhere({mode: "ghostmode"});
var defaultPage = pages.get("normalmode");

var StateModel = Backbone.Model.extend({

  initialize: function()
  {
    // update page when switching tab
    this.on("change:mode change:ghostmode change:normalmode", function()
    {
      var tab = this.getTab();
      this.set({page: tab.get("page")});
    }, this);

    // update location && is*istid when switching page
    this.on("change:page", function(_, id)
    {
      var page = pages.get(id);

      this.set({
        isFavorited: favorites.get(id) ? true : false,
        isWhitelisted: whitelist.get(id) ? true : false,
        location: page.get("location"),
        isInternal: page.get("isInternal")
      });
    }, this);

    // enable/disable back/forward on back/forward
    this.listenTo(tabs, "change:index", function(tab, index)
    {
      this.set({
        canBack: index > 0,
        canForward: index < tab.history.length - 1
      });
    });

    // enable/disable back/forward on change tab
    this.on("change:normalmode change:ghostmode", function(_, cid)
    {
      var tab = tabs.get(cid);
      var index = tab.get("index");

      this.set({
        canBack: index > 0,
        canForward: index < tab.history.length - 1
      });
    });

    // update page when tab page changes
    tabs.on("change:page", function(changedTab, pageId)
    {
      var activeTab = this.getTab();
      var activePage = this.getPage();

      if (activeTab.cid === changedTab.cid && activePage.id !== pageId)
      {
        this.set({page: pageId});
      }
    }, this);

    this.listenTo(favorites, "add remove", function(page)
    {
      this.set("isFavorited", favorites.get(page.id) ? true : false);
    });

    this.listenTo(whitelist, "add remove", function(page)
    {
      this.set("isWhitelisted", whitelist.get(page.id) ? true : false);
    });

    // keep tab history

    this.normalhistory = [this.get("normalmode")];

    this.on("change:normalmode", function(_, cid)
    {
      this.normalhistory.push(cid);
    });

    this.ghosthistory = [];

    this.on("change:ghostmode", function(_, cid)
    {
      this.ghosthistory.push(cid);
    });

    // remove item from tab history when removed from tabs

    this.listenTo(tabs, "remove", function(tab)
    {
      if (tab.get("mode") === this.MODE.GHOST)
      {
        this.ghosthistory = _.without(this.ghosthistory, tab.cid);
      }
      else
      {
        this.normalhistory = _.without(this.normalhistory, tab.cid);
      }
    });
  },

  MODE: {
    NORMAL: "normalmode",
    GHOST: "ghostmode"
  },

  VIEW: {
    NORMAL: "normaltabsview",
    GHOST: "ghosttabsview",
    BOOKMARKS: "bookmarksview",
    HISTORY: "historyview",
    BROWSER: "browserview"
  },

  defaults: {
    mode: "normalmode",
    view: "browserview",
    normalmode: defaultNormalTab.cid,
    ghostmode: defaultGhostTab.cid,
    page: defaultPage.cid,
    location: defaultPage.get("location"),
    isInternal: defaultPage.get("isInternal"),
    isMenuOpen: false,
    isFavorited: false,
    isWhitelisted: false,
    canBack: false,
    canForward: false
  },

  // convinience methods =====================================================//

  getTab: function()
  {
    // this.get(this.get("mode")) is the active tab cid in the active mode
    return tabs.get(this.get(this.get("mode")));
  },

  getPage: function()
  {
    return pages.get(this.get("page"));
  },

  gotoLocation: function(location)
  {
    // find page based on location or create a new page
    var page = pages.findWhere({location: location}) || pages.createItem();
    var tab = this.getTab();

    tab.set({page: page.id});
  },

  toggleMenu: function()
  {
    this.set({
      isMenuOpen: !this.get("isMenuOpen")
    });
  },

  toggleFavorite: function()
  {
    if (this.get("isFavorited"))
    {
      favorites.remove(this.getPage());
    }
    else
    {
      favorites.add(this.getPage());
    }
  },

  toggleWhitelisted: function()
  {
    if (this.get("isWhitelisted"))
    {
      whitelist.remove(this.getPage());
    }
    else
    {
      whitelist.add(this.getPage());
    }
  }

});

module.exports = new StateModel();
