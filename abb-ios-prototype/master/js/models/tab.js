
var Backbone = require("backbone");

module.exports = Backbone.Model.extend({

  initialize: function()
  {
    this.history = [this.get("mode")];

    this.on("change:page", function(_, id)
    {
      var index = this.get("index");

      // if not refresh / navigating to the same page
      if (this.history[index] !== id)
      {
        // if not at the end of the history (user has gone back)
        if (index !== this.history.length - 1)
        {
          // start overwriting history from this point on
          this.history = this.history.slice(0, index + 1);
        }

        this.history.push(id);
        this.set({index: this.history.length - 1});
      }
    });
  },

  goBack: function()
  {
    if (this.get("index") > 0)
    {
      this.set({index: this.get("index") - 1});
      this.set({page: this.history[this.get("index")]});
    }
  },

  goForward: function()
  {
    if (this.get("index") < this.history.length - 1)
    {
      this.set({index: this.get("index") + 1});
      this.set({page: this.history[this.get("index")]}, {trigger: true});
    }
  },

  defaults: {
    mode: "normalmode",
    page: "normalmode",
    index: 0
  }

});
