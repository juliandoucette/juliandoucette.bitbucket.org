
var Backbone = require("backbone");

var PageModel = Backbone.Model.extend({

  defaults: {
    title: "Page 0",
    location: "page0.com",
    isInternal: false,
    src: "pages/example/index.html"
  },

  initialize: function()
  {
    // blindly replace "0"s with model.id
    for (var prop in this.attributes)
    {
      if (typeof this.attributes[prop] === "string")
      {
        this.attributes[prop] = this.attributes[prop].replace("0", this.id);
      }
    }
  }

});

module.exports = PageModel;
