
var _ = require("underscore");

_.templateSettings = {
  evaluate: /{{([\s\S]+?)}}/g,
  interpolate: /{{=([\s\S]+?)}}/g,
  escape: /{{-([\s\S]+?)}}/g
};

module.exports = window.abb = {
  models: {
    page: require("./models/page"),
    state: require("./models/state"),
    tab: require("./models/tab")
  },
  collections: {
    favorites: require("./collections/favorites"),
    history: require("./collections/history"),
    pages: require("./collections/pages"),
    tabs: require("./collections/tabs"),
    whitelist: require("./collections/whitelist")
  },
  views: {
    addressbar: require("./views/addressbar"),
    bookmarks: require("./views/bookmarks"),
    container: require("./views/container"),
    history: require("./views/history"),
    iframes: require("./views/iframes"),
    menu: require("./views/menu"),
    tabs: require("./views/tabs"),
    tapbar: require("./views/tapbar")
  }
};
